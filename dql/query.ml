(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(* TODO: Add Lifetime as a built-in datatype. *)

open Rapids
;;

module Formula =
  struct
    type 'a formula =
    | And of 'a formula list
    | Or of 'a formula list
    | Not of 'a formula
    | True
    | False
    | Atom of 'a
    ;;

    let rec map g = function
    | And fl -> And(List.map (map g) fl)
    | Or fl -> Or(List.map (map g) fl)
    | Not f -> Not(map g f)
    | True -> True
    | False -> False
    | Atom x -> Atom(g x)
    ;;
  end
;;

open Formula;;

module Date_set =
  Set.Make(
    struct
      type t = Lifetime.day
      let compare = compare
    end)
;;

type date_set = Date_set.t;;

type value_ =
  | Nothing
  | String of string
  | Int of int
  | Float of float
  | Date of Lifetime.day
  | Archive_id of archive_id
  | Unit_id of unit_id
  | Package_id of package_id
  | Source_id of source_id
  | Version of (version_number * release_number)
  | Date_set of date_set
  | Package_set of package_set
  | Unit_set of unit_set
  | Source_set of source_set
  | Archive_set of archive_set
  | Bool of bool
  | Formula of value_ formula
  | Array of value_ array
  | List of value_ list
  | Thunk of (value_ -> value_)
  | Regexp of Pcre.regexp
  | Record of (string * value_) list
  | Versioned of (unit_id, version_number * release_number, glob) Napkin.versioned
  | Diagnosis of
      (Package_set.elt, Package_set.elt, (unit_id, version_number * release_number, glob) Napkin.versioned) Diagnosis.diagnosis
  | Diagnosis_list of
      (Package_set.elt list, Package_set.elt, (unit_id, version_number * release_number, glob) Napkin.versioned) Diagnosis.diagnosis
;;

type variable_name = string;;

type comparison =
| Eq
| Le
| Lt
| Gt
| Ge
| Ne
;;

type 'a resolution = string * 'a option ref;;

type ternary =
| Ternary_operator of (value_ -> value_ -> value_ -> value_) resolution
| Find_package
| Build_versioned
| Strong_dep
| Dep_path
;;

type binary =
| Binary_operator of (value_ -> value_ -> value_) resolution
| Comparison of comparison
| Union
| Intersection
| Diff
| Find_source
| Match
| Filter
| Forall
| Exists
| Member
| Treat
| Map
| Check (** Returns a diagnosis for a set of packages [indivitually]  *)
| Check_together (** Returns a diagnostic for a set of packages [together] *)
| Install (** Returns an installation for a given set of packages *)
| Encode (** Returns an encoding for the installation for a given set of packages *)
| Difficulty (** Returns an integer indicating the computational difficulty of finding an installation for a given set of packages *)
| Contents (** Contents of an archive *)
;;

type unary =
| Unary_operator of (value_ -> value_) resolution
| Complement
| Select                (** Select the packages matching a versioned *)
| Find_unit             (** Find a unit of a given name *)
| Find_version          (** Find a version *)
| Find_archive          (** Find an archive *)
| Unit                  (** Set of units of a set of packages or of a set of sources *)
| From_unit             (** Set of packages of a set of units *)
| Source                (** Set of sources of a set of packages *)
| Success
| From_source           (** Set of packages of a set of sources *)
| Provides              (** Returns the set of units this package provides *)
| Versions              (** Returns the set of packages implementing directly this unit *)
| Conflicts             (** Returns the set of packages this package conflicts with *)
| Conflict_list         (** Returns the list of conflicts *)
| Replaces              (** Returns the set of packages this package replaces *)
| Latest                (** Returns the latest version of a unit *)
| Count                 (** Returns the cardinality of a set or the length of a list *)
| Depends               (** Returns the dependencies of a package *)
| Pre_depends           (** Returns the pre-dependencies of a package *)
| Range                 (** Range of dates of an archive *)
| Closure               (** Returns the pre-dependency or dependency closure of a package *)
| Dep_closure           (** Returns the dependency closure of a package *)
| Pre_closure           (** Returns the pre-dependency closure of a package *)
| What_provides         (** Returns the set of packages that provide a given unit *)
| Singleton             (** Create a singleton set *)
| Requires              (** Set of packages *possibly* requiring a given set of packages *)
| Is_empty              (** Returns true iff. the set is empty *)
| Is_essential          (** Returns true iff. package is essential *)
| Is_build_essential    (** Returns true iff. package is build-essential *)
| Load_packages         (** Load set of packages from file *)
| Temperature           (** Temperature and statistics of a CNF formula *)
| Size                  (** Size of a package *)
| Installed_size        (** Installed size of a package *)
| Solve                 (** Solve a formula *)
| Trim                  (** Returns the set of installable packages *)
| Elements              (** elements of a list *)
;;

type nonary =
| Nonary_operator of (unit -> value_) resolution
| Packages              (** Set of all packages *)
| Units                 (** Set of all units *)
| Sources               (** Set of all sources *)
| Archives              (** Set of all archives *)
;;

type expression =
| Content of location
| Nonary of nonary
| Unary of unary * expression
| Binary of binary * expression * expression
| Ternary of ternary * expression * expression * expression
| Constant of value_
| Apply of expression * expression
| List_constructor of expression list
| Set_constructor of expression list
| Formula_constructor of expression formula
| Lambda of variable_name * expression
and location =
| Variable of variable_name
| Index of expression * expression
;;

type statement =
| Assignment of location * expression
| Directive of string * expression list
| Nop
;;

type program = statement list;;

type query =
| Statement of statement
| Expression of expression
;;
