(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt and the EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

class reason1 : reason =
  object(self)
    method print fmt = Format.fprintf fmt "shit"
  end
;;

class reason2 : reason =
  object(self)
    method print fmt = Format.fprintf fmt "shit"
  end
;;

exception Conflict of reason;;

class solver () =
  object(self)
    val mutable stuff = []
    method add r = stuff <- r :: stuff
    method get = stuff
    method shit =
      match stuff with
      | r1::_ -> raise (Conflict r1)
      | _ -> false
  end
;;
