(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Conduit
open Napkin
open Rapids
open Diagnosis
open Query
open Formula
;;

exception Parse_error of int * int * string
exception Unbound_value of string
exception Unknown_directive of string
exception Sorry of string
exception Type_error 
exception Because of string * exn
exception Time_limit_exceeded
exception Space_limit_exceeded
exception Output_limit_exceeded
exception Access_denied
exception Not_implemented
exception Quit
;;

module SM = Map.Make(String)
module SS = Set.Make(String)
;;

type env = value_ SM.t;;

(*** context *)
type context = {
  ctx_space_limit : int;
  ctx_time_limit : int;
  ctx_output_limit : int;
  ctx_filesystem_access : bool;
  ctx_modify_database : bool;
  ctx_modify_environment : bool;
  ctx_interactive : bool;    
  ctx_default_architecture : string;
}
;;
(* ***)
(*** 'a t *)
type 'a t = {
  mutable db : db;
  cx : context;
  cd : 'a conduit;
  mutable tm : int; (* Remaining time *)
  mutable sp : int; (* Remaining space *)
  mutable ol : int; (* Remaining output space *)
  mutable env : env (* Variable environment *)
}
;;
(* ***)

let default_architecture = "i386";; (* Hard-coded *)

(*** default_batch_context *)
let default_batch_context ?(architecture=default_architecture) () = {
  ctx_space_limit          = max_int;     
  ctx_time_limit           = max_int;      
  ctx_output_limit         = max_int;
  ctx_filesystem_access    = true; 
  ctx_modify_database      = true; 
  ctx_modify_environment   = true;
  ctx_interactive          = false; 
  ctx_default_architecture = architecture;
};;
(* ***)
(*** default_cli_context *)
let default_cli_context ?architecture () = {
  (default_batch_context ?architecture ())
  with
  ctx_interactive = true;
  ctx_output_limit = 100;
};;
(* ***)
(*** default_web_context *)
let default_web_context ?(architecture=default_architecture) () = {
  ctx_space_limit          = 100_000;
  ctx_time_limit           = 10_000;
  ctx_output_limit         = 100_000;
  ctx_filesystem_access    = false;
  ctx_modify_database      = false;
  ctx_modify_environment   = false;
  ctx_interactive          = false;
  ctx_default_architecture = architecture;
};;
(* ***)

let env0 = SM.empty;;

(*** create_interpreter *)
let create_interpreter db cx cd =
  { db = db;
    cx = cx;
    cd = cd;
    tm = 0;
    sp = 0;
    ol = 0;
    env = env0 }
;;
(* ***)

let sf = Printf.sprintf;;

(*** apply_parser *)
let apply_parser p u =
  let l = Lexing.from_string u in
  try
    p Lexic.token l
  with
  | Syntax.Error ->             raise (Parse_error(Lexing.lexeme_start l,Lexing.lexeme_end l,"Syntax error"))
  | Parsing.Parse_error ->      raise (Parse_error(Lexing.lexeme_start l,Lexing.lexeme_end l,"Parse error"))
  | Failure x ->                raise (Parse_error(Lexing.lexeme_start l,Lexing.lexeme_end l,"Failure: "^x))
  | Lexic.Parse_error(i,j,x) -> raise (Parse_error(i,j,x))
;;
(* ***)

let parse_expression   = apply_parser Syntax.expression
let parse_query        = apply_parser Syntax.query
let parse_program      = apply_parser Syntax.program
;;

(*** scribe_date *)
let scribe_date cd oc j =
  let (y,m,d) = Lifetime.ymd_of_day j in
  cd.cd_print oc "%04d-%02d-%02d" y m d
;;
(* ***)
(*** package_set_of_value *)
let package_set_of_value = function
  | Package_set ps -> ps
  | _ -> raise (Because ("Package set expected", Type_error))
;;
(* ***)
(*** diagnosis_of_value *)
let diagnosis_of_value = function
  | Diagnosis dg -> dg
  | _ -> raise (Because ("Diagnosis expected", Type_error))
;;
(* ***)
(*** package_id_of_value *)
let package_id_of_value = function
  | Package_id p_id -> p_id
  | _ -> raise (Because ("Package expected", Type_error))
;;
(* ***)
(*** int_of_value *)
let int_of_value = function
  | Int x -> x
  | _ -> raise (Because ("Integer expected", Type_error))
;;
(* ***)
(*** bool_of_value *)
let bool_of_value = function
	| Bool b -> b
	| _ -> raise (Because ("Boolean expected", Type_error))
;;
(* ***)
(*** source_id_of_value *)
let source_id_of_value = function
  | Source_id s_id -> s_id
  | _ -> raise (Because ("Source package expected", Type_error))
(* ***)
(*** archive_id_of_value *)
let archive_id_of_value = function
  | Archive_id a_id -> a_id
  | _ -> raise (Because ("Archive expected", Type_error))
;;
(* ***)
(*** date_of_value *)
let date_of_value = function
  | Date j -> j
  | _ -> raise (Because ("Date expected", Type_error))
;;
(* ***)
(*** unit_id_of_value *)
let unit_id_of_value = function
  | Unit_id u_id -> u_id
  | _ -> raise (Because ("Unit expected", Type_error))
;;
(* ***)
(*** string_of_value *)
let string_of_value = function
  | String u -> u
  | _ -> raise (Because ("String expected", Type_error))
;;
(* ***)
(*** version_of_value *)
let version_of_value = function
  | Version (v, r) -> v, r
  | _ -> raise (Because ("Version number expected", Type_error))
;;
(* ***)
(*** unit_id_of_string *)
let unit_id_of_string it u =
  try
    Unit_index.search (get_unit_index it.db) u
  with
  | Not_found -> raise (Sorry(sf "Can't find unit %S" u))
;;
(* ***)
(*** archive_id_of_string *)
let archive_id_of_string it a =
  try
    Archive_index.search (get_archive_index it.db) a
  with
  | Not_found -> raise (Sorry(sf "Can't find archive %S" a))
;;
(* ***)
(*** version_of_string *)
let version_of_string it x =
	let (v, r) = Rapids.split_version x in
  let version_index = get_version_index it.db 
	and release_index = get_release_index it.db in
  try
    if it.cx.ctx_modify_database then
      (Version_index.register version_index v,
			Release_index.register release_index r)
    else
      (Version_index.search version_index v,
			Release_index.search release_index r)
  with
  | Not_found -> raise (Sorry(sf "Can't find version %S" x))
;;
(* ***)
(*** reset_output_limit *)
let reset_output_limit it =
  it.ol <- it.cx.ctx_output_limit
;;
(* ***)
(*** reset_limits *)
let reset_limits it =
  it.tm <- it.cx.ctx_time_limit;
  it.sp <- it.cx.ctx_space_limit;
  reset_output_limit it
;;
(* ***)
(*** tick *)
let tick it =
  if it.tm > 0 then
    it.tm <- it.tm - 1
  (* else
    raise Time_limit_exceeded *)
;;
(* ***)
(*** tick_out *)
let tick_out it =
  tick it;
  if it.ol > 0 then
    it.ol <- it.ol - 1
  else
    raise Output_limit_exceeded
;;
(* ***)
(*** execute_statement *)
let execute_statement it s =
  tick it;
;;
(* ***)
(*** Nonary_library *)
module Nonary_library =
  struct
    let units it () = Unit_set(Functions.units it.db);;
    let packages it () = Package_set(Functions.packages it.db);;
    let sources it () = Source_set(Functions.sources it.db);;
    let archives it () = Archive_set(Functions.archives it.db);;

    let table = Hashtbl.create 1009;;

    let resolve it = function
      | Nonary_operator(name, q) ->
          begin
            match !q with
            | None ->
                begin
                  try
                    let f = Hashtbl.find table in
                    q := Some f;
                    f
                  with
                  | Not_found -> raise (Unbound_value name)
                end
            | Some f -> f
          end
      | Units -> units it
      | Packages -> packages it
      | Sources -> sources it
      | Archives -> archives it
      (*| _ -> raise Not_implemented*)
    ;;
  end
;;
(* ***)
(*** Unary_library *)
module Unary_library =
  struct
    (*** complement *)
    let complement it = function
    | Bool b -> Bool(not b)
    | _ -> raise (Because ("not: Boolean expected", Type_error))
    ;;
    (* ***)
    (*** find_unit *)
    let find_unit it u' = Unit_id(unit_id_of_string it (string_of_value u'));;
    (* ***)
    (*** find_archive *)
    let find_archive it a' = Archive_id(archive_id_of_string it (string_of_value a'));;
    (* ***)
    (*** select *)
    let select it = function
      | Versioned vd -> Package_set(Functions.select it.db vd)
      | _ -> raise (Because ("selection: Version specification expected", Type_error))
    ;;
    (* ***)
    (*** find_version *)
    let find_version it v' =
			Version(version_of_string it (string_of_value v'));;
    (* ***)
    (*** count *)
    let count it = function
    | Package_set s -> Int(Package_set.cardinal s)
    | Unit_set    s -> Int(Unit_set.cardinal s)
    | Source_set  s -> Int(Source_set.cardinal s)
    | Date_set    s -> Int(Date_set.cardinal s)
    | Archive_set s -> Int(Archive_set.cardinal s)
    | _ -> raise (Because ("count: Set expected", Type_error))
    ;;
    (* ***)
    (*** range *)
    let range it v =
      let db = it.db in
      let a_id = archive_id_of_value v in
      let archive_index = get_archive_index db in
      let a = Archive_index.data archive_index a_id in
      let (j0,j1) = archive_range db a in
      List[Date j0; Date j1]
    ;;
    (* ***)
    (*** closure *)
    let closure relations it v =
      let db = it.db in
      let ps = package_set_of_value v in
      let ps' = Functions.dependency_closure db ~relations ps in
      Package_set ps'
    ;;
    (* ***)
    (*** conflicts *)
    let conflicts it v =
      let db = it.db in
      let ps = package_set_of_value v in
      let ps' = Functions.conflicts db ps in
      Package_set ps'
    ;;
    (* ***)
    (*** conflicts *)
    let conflicts it v =
      let db = it.db in
      let ps = package_set_of_value v in
      let ps' = Functions.conflicts db ps in
      Package_set ps'
    ;;
    (* ***)
    (*** conflict_list *)
    let conflict_list it v =
      let p_id = package_id_of_value v in
      let db = it.db in
      let p = Functions.get_package_from_id db p_id in
      List(
        List.map
          begin fun v ->
            Versioned v
          end
          p.pk_conflicts)
    ;;
    (* ***)
    (*** what_provides *)
    let what_provides it v =
      let u_s =
        match v with
        | Unit_set u_s -> u_s
        | Unit_id u_id -> Unit_set.singleton u_id
        | _ -> raise (Because ("what_provides: Unit or unit set expected", Type_error))
      in
      Package_set
        begin
          Unit_set.fold
            begin fun u_id p_s ->
							let x = ref Package_set.empty in
							List.iter (fun spec ->
								match spec with
								| Unit_version (pkg, _) -> x := Package_set.add pkg !x
								| Glob_pattern _ -> raise (Invalid_argument "Uh?")
							) (Functions.unit_id_to_providers it.db u_id);
							Package_set.union p_s !x
            end
            u_s
            Package_set.empty
        end
    ;;
    (* ***)
    (*** provides *)
    let provides it v =
      let p_id = package_id_of_value v in
      let db = it.db in
      let p = Functions.get_package_from_id db p_id in
      List(
        List.map
          begin fun v ->
            Versioned v
          end
          p.pk_provides)
    ;;
    (* ***)
    (*** depends *)
    let depends it v =
      let p_id = package_id_of_value v in
      let p = Functions.get_package_from_id it.db p_id in
      List(List.map
        begin fun disjunction ->
          List(List.map (fun v -> Versioned v) disjunction)
        end
        p.pk_depends)
    ;;
    (* ***)
		let pre_depends it v =
      let p_id = package_id_of_value v in
      let p = Functions.get_package_from_id it.db p_id in
      List(List.map
        begin fun disjunction ->
          List(List.map (fun v -> Versioned v) disjunction)
        end
        p.pk_pre_depends)
    ;;
		(*** load_packages *)
		let load_packages it v =
		let channel =
			match v with 
				String s -> open_in s 
			| _ -> raise (Because ("load_packages: File name expected", Type_error))
		in
		begin
			let set = ref Package_set.empty in
			let db = it.db in
			let arch_index = get_architecture_index db 
			and package_index = get_package_index db
			and version_index = get_version_index db
			and release_index = get_release_index db
			and unit_index = get_unit_index db in
			begin
			try
				while true
				do
					let l = input_line channel in
					Scanf.sscanf l "%[^']'%[^@]@@%s" (fun u x a ->
					let (v, r) = Rapids.split_version x in
					try let u_id = Unit_index.search unit_index u in
						try let a_id = Architecture_index.search arch_index (if a = "" then it.cx.ctx_default_architecture else a) in
							try let v_id = Version_index.get_id (Version_index.search version_index v) in
							try let r_id = Release_index.get_id (Release_index.search release_index r) in
								try let p_id = Package_index.search1 package_index (u_id, v_id, r_id, a_id) in
									set := Package_set.add p_id !set
								with
								| Not_found -> raise (Sorry (sf "Can't find package %s'%s@%s" u x a))
							with
							| Not_found -> raise (Sorry (sf "Can't find version %S" x))
							with
							| Not_found -> raise (Sorry (sf "Can't find version %S" x))
			
						with
						| Not_found -> raise (Sorry (sf "Can't find architecture %S" a))
					with
					| Not_found -> raise (Sorry (sf "Can't find unit %S" u)))
				done
			with
				End_of_file -> close_in channel
			end;
			Package_set !set
		end;;
		(* ***)
		(*** success *)
		let success it v =
		begin
			match v with
			| Diagnosis dg -> Bool (Array.length dg.dg_failures = 0)
			| _ -> raise (Because ("success: Diagnosis expected", Type_error))
		end;;
		(* ***)
		(*** unit_of *)
		let unit_of it v =
		let p_id = package_id_of_value v in
		let p = Functions.get_package_from_id it.db p_id in
			Unit_id p.pk_unit;;
		(* ***)
		(*** source_of *)
		let source_of it v =
		let p_id = package_id_of_value v in
		let p = Functions.get_package_from_id it.db p_id in
			Source_id p.pk_source;;
		(* ***)

		let size_of it v =
		let p_id = package_id_of_value v in
		let p = Functions.get_package_from_id it.db p_id in
			Int (Int64.to_int p.pk_size);;
			
		let installed_size_of it v =
		let p_id = package_id_of_value v in
		let p = Functions.get_package_from_id it.db p_id in
			Int (Int64.to_int p.pk_installed_size);;
	
		let elements_of it v =
		match v with
		| Package_set s ->
		  List (List.map (fun p -> Package_id p) (Package_set.elements s))
		| Unit_set    s ->
		  List (List.map (fun u -> Unit_id u) (Unit_set.elements s))
		| Source_set  s ->
		  List (List.map (fun i -> Source_id i) (Source_set.elements s))
		| Date_set    s ->
		  List (List.map (fun d -> Date d) (Date_set.elements s))
		| Archive_set s ->
		  List (List.map (fun a -> Archive_id a) (Archive_set.elements s))
		| _ -> raise (Because ("elements: Set expected", Type_error));;

		let is_empty it v =
		match v with
		| Package_set s -> Bool (Package_set.is_empty s)
		| Unit_set    s -> Bool (Unit_set.is_empty s)
		| Source_set  s -> Bool (Source_set.is_empty s)
		| Date_set    s -> Bool (Date_set.is_empty s)
		| Archive_set s -> Bool (Archive_set.is_empty s)
		| _ -> raise (Because ("is_empty: Set expected", Type_error));;

		let trim it v =
		match v with
		| Package_set s -> Package_set (Installability.trim it.db s)
		| _ -> raise (Because ("trim: package set expected", Type_error));;

		let versions_of it v =
		match v with
		| Unit_id u_id -> Package_set (Functions.unit_id_to_package_set it.db u_id)
		| _ -> raise (Because ("versions: unit expected", Type_error));;

    let table = Hashtbl.create 1009;;

    let resolve it = function
      | Unary_operator(name, q) ->
          begin
            match !q with
            | None ->
                begin
                  try
                    let f = Hashtbl.find table in
                    q := Some f;
                    f
                  with
                  | Not_found -> raise (Unbound_value name)
                end
            | Some f -> f
          end
      | Complement -> complement it
      | Find_unit -> find_unit it
      | Find_archive -> find_archive it
      | Find_version -> find_version it
      | Count -> count it
      | Select -> select it
      | Range -> range it
      | Closure -> closure [`Pre;`Dep] it
      | Pre_closure -> closure [`Pre] it
      | Dep_closure -> closure [`Dep] it
      | Conflicts -> conflicts it
      | Conflict_list -> conflict_list it
      | What_provides -> what_provides it
      | Provides -> provides it
      | Depends -> depends it
			| Pre_depends -> pre_depends it
			| Load_packages -> load_packages it
			| Success -> success it
			| Unit -> unit_of it
			| Source -> source_of it
			| Size -> size_of it
			| Installed_size -> installed_size_of it
			| Elements -> elements_of it
			| Is_empty -> is_empty it
			| Versions -> versions_of it
      | Trim -> trim it
      | _ -> raise Not_implemented
    ;;
  end
;;
(* ***)
(*** Binary_library *)
module Binary_library =
  struct
    (*** intersection *)
    let intersection it v1 v2 = match (v1,v2) with
    | Package_set ps1, Package_set ps2 -> Package_set(Package_set.inter ps1 ps2)
    | Unit_set us1,    Unit_set us2    -> Unit_set(Unit_set.inter us1 us2)
    | Source_set ss1,  Source_set ss2  -> Source_set(Source_set.inter ss1 ss2)
    | Archive_set ss1, Archive_set ss2 -> Archive_set(Archive_set.inter ss1 ss2)
    | Bool b1,         Bool b2         -> Bool(b1 && b2)
    | Date_set ds1,    Date_set ds2    -> Date_set(Date_set.inter ds1 ds2)
    | (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _), Nothing -> Nothing
    | Nothing, (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _) -> Nothing
    | Nothing, Nothing -> Nothing
    | _, _ -> raise (Because ("&: Set or boolean expected", Type_error))
    ;;
    (* ***)
    (*** union *)
    let union it v1 v2 = match (v1,v2) with
    | Package_set ps1, Package_set ps2 -> Package_set(Package_set.union ps1 ps2)
    | Unit_set us1,    Unit_set us2    -> Unit_set(Unit_set.union us1 us2)
    | Source_set ss1,  Source_set ss2  -> Source_set(Source_set.union ss1 ss2)
    | Archive_set as1, Archive_set as2 -> Archive_set(Archive_set.union as1 as2)
    | Date_set ds1,    Date_set ds2    -> Date_set(Date_set.union ds1 ds2)
    | (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _), Nothing -> v1
    | Nothing, (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _) -> v2
    | Nothing, Nothing -> Nothing
    | Bool b1,         Bool b2         -> Bool(b1 or b2)
    | _, _ -> raise (Because ("|: Set or boolean expected", Type_error))
    ;;
    (* ***)
    (*** diff *)
    let diff it v1 v2 = match (v1,v2) with
    | Package_set ps1, Package_set ps2 -> Package_set(Package_set.diff ps1 ps2)
    | Unit_set us1,    Unit_set us2    -> Unit_set(Unit_set.diff us1 us2)
    | Source_set ss1,  Source_set ss2  -> Source_set(Source_set.diff ss1 ss2)
    | Archive_set as1, Archive_set as2 -> Archive_set(Archive_set.diff as1 as2)
    | Date_set ds1,    Date_set ds2    -> Date_set(Date_set.diff ds1 ds2)
    | Bool b1,         Bool b2         -> Bool(b1 && not b2)
    | (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _), Nothing -> v1
    | Nothing, (Package_set _|Unit_set _|Source_set _|Archive_set _|Date_set _) -> Nothing
    | Nothing, Nothing -> Nothing
    | _, _ -> raise (Because ("\\: Set or boolean expected", Type_error))
    ;;
    (* ***)
		(*** member *)
		let member v1 v2 = match (v1, v2) with
		| _, Nothing -> Bool false
		| Package_id p_id, Package_set ps -> Bool (Package_set.mem p_id ps)
		| Unit_id u_id, Unit_set us -> Bool (Unit_set.mem u_id us)
		| Source_id s_id, Source_set ss -> Bool (Source_set.mem s_id ss)
		| Archive_id a_id, Archive_set ars -> Bool (Archive_set.mem a_id ars)
		| Date d, Date_set ds -> Bool (Date_set.mem d ds)
		| _, _ -> raise (Because ("member: element and set expected", Type_error))
		(* ***)
    (*** regexp_match *)
    let regexp_match it v1 v2 =
      match v1 with
      | Regexp rex ->
          begin
            let b = Buffer.create 128 in
            let cd = conduit_of_buffer b in
            let f u = Pcre.pmatch ~rex u in
            let matcher filter scribe set =
              filter
                begin fun id ->
                  Buffer.clear b;
                  scribe it.db cd cd.cd_out_channel id;
                  f (Buffer.contents b)
                end
                set
            in
            match v2 with
            | Package_set s ->
                Package_set(
                  matcher Package_set.filter
                    (fun db cd oc p_id ->
                      Functions.scribe_package_from_id db cd oc p_id)
                    s)
            | Unit_set s ->    Unit_set(matcher Unit_set.filter Functions.scribe_unit_from_id s)
            | Source_set s ->  Source_set(matcher Source_set.filter Functions.scribe_source_from_id s)
            | Archive_set s -> Archive_set(matcher Archive_set.filter Functions.scribe_archive_from_id s)
            | Date_set s ->    Date_set(matcher Date_set.filter (fun _ cd oc -> scribe_date cd oc) s)
            | _ -> raise (Because ("regexp: Set expected as first argument", Type_error))
          end
      | _ -> raise (Because ("regexp: Regular expression expected as second argument", Type_error))
    ;;
    (* ***)
    (*** find_source *)
    let find_source it v1 v2 =
      let db = it.db in
      let u = string_of_value v1
      and v = string_of_value v2
      in
      let source_index = get_source_index db in
      try
        let s_id = Source_index.search source_index (u,v) in
        Source_id s_id
      with
      | Not_found -> raise (Sorry(sf "Can't find source %s`%s" u v))
    ;;
    (* ***)
    (*** contents *)
    let contents it v1 v2 =
      let a_s =
        match v1 with
        | Archive_id a_id -> Archive_set.singleton a_id
        | Archive_set a_s -> a_s
        | _ -> raise (Because ("contents: Archive or archive set expected as first argument", Type_error))
      in
      let j_s =
        match v2 with
        | Date j -> Date_set.singleton j
        | Date_set j_s -> j_s
        | _ -> raise (Because ("contents: Date or date set expected as second argument", Type_error))
      in
      let archive_index = get_archive_index it.db in
      let ps =
        Archive_set.fold
          begin fun a_id set ->
            let a = Archive_index.data archive_index a_id in
            Date_set.fold
              begin fun j set ->
                Package_set.union (get_archive_contents a j) set
              end
              j_s
              set
          end
          a_s
          Package_set.empty
      in
      Package_set ps
    ;;
    (* ***)
    (*** check *)
    let check it v1 v2 =
      (* let module S = Solver in *)
      let db = it.db in
      let targets = package_set_of_value v1 in
      let available = package_set_of_value v2 in
			let ind = new Progress.formatter_indicator ~decimation:1000 ~label:"Solving" ~fmt:(Format.std_formatter) () in
      Diagnosis
        begin
          Installability.check
            db
            (* ~log:
               begin fun msg -> 
                 it.cd.cd_print it.cd.cd_out_channel
                   "Solver: %s\n%!" msg
               end *)
            ~indicator:ind
            ~targets
            ~available
            ()
        end;;
		(* ***)
	
		(*** check_together *)
    let check_together it v1 v2 =
      (* let module S = Solver in *)
      let db = it.db in
      let targets = package_set_of_value v1 in
      let available = package_set_of_value v2 in
			let ind = new Progress.formatter_indicator ~decimation:1000 ~label:"Solving" ~fmt:(Format.std_formatter) () in
      Diagnosis_list
        begin
          Installability.check_together
            db
            (* ~log:
               begin fun msg -> 
                 it.cd.cd_print it.cd.cd_out_channel
                   "Solver: %s\n%!" msg
               end *)
            ~indicator:ind
            ~targets
            ~available
            ()
        end;;
		(* ***)
		(*** install *)
		let install it v1 v2 =
      (* let module S = Solver in *)
      let db = it.db in
      let targets = package_set_of_value v1 in
      let available = package_set_of_value v2 in
			let ind = new Progress.formatter_indicator ~decimation:1000 ~label:"Solving" ~fmt:(Format.std_formatter) () in
			Package_set (Installability.install
				db
         (* ~log:
             begin fun msg ->
              it.cd.cd_print it.cd.cd_out_channel
                "Solver: %s\n%!" msg
            end *)
				~indicator:ind
				~targets
				~available
				()
			 )
			;;
    (* ***)

		(*** andify_value_list *)
		let rec andify_value_list vl =
		begin
			match vl with
				[] -> true
			| h::t -> (match h with
					Bool b -> if b then (andify_value_list t) else false
				| _ -> raise (Because ("function should return booleans", Type_error)))
		end;;
		(* ***)
		(*** orify_value_list *)
		let rec orify_value_list vl =
		begin
			match vl with
				[] -> false
			| h::t -> (match h with
					Bool b -> if b then true else (orify_value_list t)
				| _ -> raise (Because ("function should return booleans", Type_error)))
		end;;
		(* ***)
		(*** iterate *)
		let iterate ifier v1 v2 =
		begin
			match v1 with
			| Date_set d_s ->
				begin
					match v2 with
					| Thunk f -> Bool (ifier (List.map (fun d -> f (Date d)) (Date_set.elements d_s)))
					| _ -> raise (Because ("Function expected as second argument", Type_error))
				end
			|	Package_set p_s ->
				begin
					match v2 with
					| Thunk f -> Bool (ifier (List.map (fun p -> f (Package_id p)) (Package_set.elements p_s)))
					| _ -> raise (Because ("Function expected as second argument", Type_error))
				end
			| Unit_set u_s ->
				begin
					match v2 with
					| Thunk f -> Bool (ifier (List.map (fun u -> f (Unit_id u)) (Unit_set.elements u_s)))
					| _ -> raise (Because ("Function expected as second argument", Type_error))
				end
			| Source_set s_s ->
				begin
					match v2 with
					| Thunk f -> Bool (ifier (List.map (fun s -> f (Source_id s)) (Source_set.elements s_s)))
					| _ -> raise (Because ("Function expected as second argument", Type_error))
				end
			| Archive_set a_s ->
				begin
					match v2 with
					| Thunk f -> Bool (ifier (List.map (fun a -> f (Archive_id a)) (Archive_set.elements a_s)))
					| _ -> raise (Because ("Function expected as second argument", Type_error))
				end
			| _ -> raise (Because ("Set expected as first argument", Type_error))
		end;;
		(* ***)
		(*** filter *)
		let filter v1 v2 =
		begin
			match v1 with
			| Date_set d_s ->
				begin
					match v2 with
					| Thunk f -> Date_set (Date_set.filter (fun d -> bool_of_value (f (Date d))) d_s)
					| _ -> raise (Because ("filter: Function expected as second argument", Type_error))
				end
			|	Package_set p_s ->
				begin
					match v2 with
					| Thunk f -> Package_set (Package_set.filter (fun p -> bool_of_value (f (Package_id p))) p_s) 
					| _ -> raise (Because ("filter: Function expected as second argument", Type_error))
				end
			| Unit_set u_s ->
				begin
					match v2 with
					| Thunk f -> Unit_set (Unit_set.filter (fun u -> bool_of_value (f (Unit_id u))) u_s)
					| _ -> raise (Because ("filter: Function expected as second argument", Type_error))
				end
			| Source_set s_s ->
				begin
					match v2 with
					| Thunk f -> Source_set (Source_set.filter (fun s -> bool_of_value (f (Source_id s))) s_s)
					| _ -> raise (Because ("filter: Function expected as second argument", Type_error))
				end
			| Archive_set a_s ->
				begin
					match v2 with
					| Thunk f -> Archive_set (Archive_set.filter (fun a -> bool_of_value (f (Archive_id a))) a_s)
					| _ -> raise (Because ("filter: Function expected as second argument", Type_error))
				end
			| _ -> raise (Because ("filter: Set expected as first argument", Type_error))
		end;;
		(* ***)
		(*** list_to_set *)
		let list_to_set l =
		begin
			match l with
			| [] -> Nothing
      | h::t -> 
				begin
					match h with
					| Unit_id _ -> Unit_set (List.fold_left (fun set x -> Unit_set.add x set) Unit_set.empty (List.map unit_id_of_value l))
					| Package_id _ -> Package_set (List.fold_left (fun set x -> Package_set.add x set) Package_set.empty (List.map package_id_of_value l))
					| Source_id _ -> Source_set (List.fold_left (fun set x -> Source_set.add x set) Source_set.empty (List.map source_id_of_value l))
					| Archive_id _ -> Archive_set (List.fold_left (fun set x -> Archive_set.add x set) Archive_set.empty (List.map archive_id_of_value l))
					| Date _ -> Date_set (List.fold_left (fun set x -> Date_set.add x set) Date_set.empty (List.map date_of_value l))
					| _ -> raise Type_error
				end
		end;;
		(* ***)
		(*** map *)
		let map v1 v2 =
		begin
			match v1 with
			| Date_set d_s ->
				begin
					match v2 with
					| Thunk f -> list_to_set (List.rev_map (fun d -> f (Date d)) (Date_set.elements d_s))
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			|	Package_set p_s ->
				begin
					match v2 with
					| Thunk f -> list_to_set (List.rev_map (fun p -> f (Package_id p)) (Package_set.elements p_s))
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			| Unit_set u_s ->
				begin
					match v2 with
					| Thunk f -> list_to_set (List.rev_map (fun u -> f (Unit_id u)) (Unit_set.elements u_s))
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			| Source_set s_s ->
				begin
					match v2 with
					| Thunk f -> list_to_set (List.rev_map (fun s -> f (Source_id s)) (Source_set.elements s_s))
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			| Archive_set a_s ->
				begin
					match v2 with
					| Thunk f -> list_to_set (List.rev_map (fun a -> f (Archive_id a)) (Archive_set.elements a_s))
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			| List l ->
				begin
					match v2 with
					| Thunk f -> List (List.map f l)
					| _ -> raise (Because ("map: Function expected as second argument", Type_error))
				end
			| _ -> raise (Because ("map: Set or list expected as first argument", Type_error))
		end;;
		(* ***)
		(*** exists *)
		let exists v1 v2 =
		begin
		end;;
		(* ***)
		(*** comparison *)
		let rec comparison it op v1 v2 =
		let rpmcmp it vn1 r1 vn2 r2 =
		let vcmp = Version_index.compare_versions (Rapids.get_version_index it.db) in	
		let rcmp = Release_index.compare_versions (Rapids.get_release_index it.db) in	
		begin
			if vcmp vn1 vn2 < 0 then -1
			else if vcmp vn1 vn2 > 0 then 1
			else (* vcmp vn1 vn2 = 0 *)
				let rs1 = Release_index.get_version r1
				and rs2 = Release_index.get_version r2 in
				begin
					if (rs1 = None) || (rs2 = None) then 0
					else if rcmp r1 r2 < 0 then -1
					else if rcmp r1 r2 > 0 then 1
					else (* rcmp r1 r2 = 0 *) 0
			end
		end in
		let fullcmp x = match op with
			Eq -> x = 0
		| Lt -> x < 0
		| Le -> x <= 0
		| Ge -> x >= 0
		| Gt -> x > 0
		| Ne -> x <> 0 in
		let somecmp x = match op with
		  Eq -> x = 0
		| Ne -> x <> 0
		| _ -> raise (Because ("This comparison has no meaning", Type_error)) in
		begin
			match v1, v2 with
			| Nothing, Nothing -> Bool true
			| String s1, String s2 -> Bool (fullcmp (compare s1 s2))
			| Int i1, Int i2 -> Bool (fullcmp (compare i1 i2))
			| Float f1, Float f2 -> Bool (fullcmp (compare f1 f2))
			| Date d1, Date d2 -> Bool (fullcmp (compare d1 d2))
			|	Bool b1, Bool b2 -> Bool (fullcmp (compare b1 b2))
			| Archive_id a1, Archive_id a2 -> Bool (somecmp (compare a1 a2))
			| Unit_id u1, Unit_id u2 -> Bool (somecmp (compare u1 u2))
			| Package_id p1, Package_id p2 -> Bool (somecmp (compare p1 p2))
			| Source_id s1, Source_id s2 -> Bool (somecmp (compare s1 s2))
			| Version (vn1, r1), Version (vn2, r2) -> Bool 
					(if Rapids.get_liquid it.db = Some RPM then
						fullcmp (rpmcmp it vn1 r1 vn2 r2)
					 else
					  fullcmp (let x = Version_index.compare_versions (Rapids.get_version_index it.db) vn1 vn2 in
					 	if x = 0 then Release_index.compare_versions (Rapids.get_release_index it.db) r1 r2 else x))
			| Date_set ds1, Date_set ds2 -> Bool (fullcmp (Date_set.compare ds1 ds2))
			| Package_set ps1, Package_set ps2 -> Bool (fullcmp (Package_set.compare ps1 ps2))
			| Unit_set us1, Unit_set us2 -> Bool (fullcmp (Unit_set.compare us1 us2))
			| Source_set ss1, Source_set ss2 -> Bool (fullcmp (Source_set.compare ss1 ss2))
			| Archive_set as1, Archive_set as2 -> Bool (fullcmp (Archive_set.compare as1 as2))
			| List l1, List l2 ->
				begin
					match op with
					| Eq -> (try
							Bool (List.fold_left2 (fun old x1 x2 -> old && bool_of_value (comparison it Eq x1 x2)) true l1 l2)
						with Invalid_argument _ -> Bool false)
					| Ne -> (try
							Bool (List.fold_left2 (fun old x1 x2 -> old || bool_of_value (comparison it Ne x1 x2)) false l1 l2)
						with Invalid_argument _ -> Bool true)
					| _ -> raise Type_error
				end
			| _, _ -> raise Type_error
		end;;
		(* ***)
    let table = Hashtbl.create 1009;;

    (*** resolve *)
    let resolve it = function
      | Binary_operator(name, q) ->
          begin
            match !q with
            | None ->
                begin
                  try
                    let f = Hashtbl.find table in
                    q := Some f;
                    f
                  with
                  | Not_found -> raise (Unbound_value name)
                end
            | Some f -> f
          end
      | Intersection -> intersection it
      | Union -> union it
      | Diff -> diff it
			| Member -> member
      | Match -> regexp_match it
      | Find_source -> find_source it
      | Contents -> contents it
      | Check -> check it
			| Check_together -> check_together it
			| Install -> install it
			| Forall -> iterate andify_value_list
			| Exists -> iterate orify_value_list
			| Filter -> filter
			| Map -> map
			| Comparison x -> comparison it x
      | _ -> raise Not_implemented
    ;;
    (* ***)
  end
;;
(* ***)
(*** Ternary_library *)
module Ternary_library =
  struct
    (*** build_versioned *)
    let build_versioned it u_id' op' v' =
      let u_id = unit_id_of_value u_id' in
      Versioned
        begin
          match op' with
          | Nothing ->
              if v' = Nothing then
                Unit_version (u_id, Sel_ANY)
              else
                raise (Because ("Comparison operator expected", Type_error))
          | String op ->
              begin
                let v = version_of_value v' in
                match op with
                | "<"  -> Unit_version (u_id, Sel_LT v)
                | "<=" -> Unit_version (u_id, Sel_LEQ v)
                | "="  -> Unit_version (u_id, Sel_EQ v)
                | ">=" -> Unit_version (u_id, Sel_GEQ v)
                | ">"  -> Unit_version (u_id, Sel_GT v)
                | _ -> raise (Sorry(sf "Unknown version comparison operator %S" op))
              end
          | _ -> raise (Because("Bad type for op", Type_error))
        end
    ;;
    (* ***)
    (*** find_package *)
    let find_package it v1 v2 v3 =
      let db = it.db in
      let architecture_index = get_architecture_index db in
      let package_index = get_package_index db in
      let version_index = get_version_index db in
      let release_index = get_release_index db in
      let unit_index = get_unit_index db in
      let (u,x,a) =
        match (v1,v2,v3) with
        | String u, String x, String a -> (u, x, a)
        | String u, String x, Nothing  -> (u, x, it.cx.ctx_default_architecture)
        | _ -> raise Type_error
      in
			let (v, r) = Rapids.split_version x in
      try
        let u_id = Unit_index.search unit_index u in
        try
          let a_id = Architecture_index.search architecture_index a in
          try
            let v_id = Version_index.get_id (Version_index.search version_index v) in
					try
						let r_id = Release_index.get_id (Release_index.search release_index r) in
            try
              let p_id = Package_index.search1 package_index (u_id, v_id, r_id, a_id) in
              Package_id p_id
            with
            | Not_found -> raise (Sorry(sf "Can't find package %s'%s@%s" u x a))
          with
          | Not_found -> raise (Sorry(sf "Can't find version %S" x))
          with
          | Not_found -> raise (Sorry(sf "Can't find version %S" x))
        with
        | Not_found -> raise (Sorry(sf "Can't find architecture %S" a))
      with
      | Not_found -> raise (Sorry(sf "Can't find unit %S" u))
    ;;
    (* ***)

		(*** check_strong_dependency *)
		let check_strong_dependency it vs v1 v2 =
		begin
			let ps = package_set_of_value vs
			and p1 = package_id_of_value v1
			and p2 = package_id_of_value v2 in
			Bool (Installability.strong_dep it.db ps p1 p2)
		end;;

		let dependency_path it vs v1 v2 =
		begin
			let ps = package_set_of_value vs
			and p1 = package_id_of_value v1
			and p2 = package_id_of_value v2 in
			match Functions.dependency_path it.db ps p1 p2 with
			| None -> Nothing
			| Some l -> List (List.map (fun p -> Package_id p) l)
		end;;

    let table = Hashtbl.create 1009;;

    (*** resolve *)
    let resolve it = function
      | Ternary_operator(name, q) ->
          begin
            match !q with
            | None ->
                begin
                  try
                    let f = Hashtbl.find table in
                    q := Some f;
                    f
                  with
                  | Not_found -> raise (Unbound_value name)
                end
            | Some f -> f
          end
      | Find_package -> find_package it
      | Build_versioned -> build_versioned it
			| Strong_dep -> check_strong_dependency it
			| Dep_path -> dependency_path it 
      (* | _ -> raise Not_implemented *)
    ;;
    (* ***)
  end
;;
(* ***)
(*** replace_free_variables *)
let replace_free_variables_of_expression ~bound ~env ~expression =
  let rec loop bound = function
    | Content(Variable v) as x ->
        begin
          if SS.mem v bound then
            x
          else
            try
              Constant(SM.find v env)
            with
            | Not_found -> raise (Unbound_value v)
        end
    | Ternary(op,x1,x2,x3) -> Ternary(op, loop bound x1, loop bound x2, loop bound x3)
    | Content(Index(x1,x2)) -> Content(Index(loop bound x1, loop bound x2))
    | Binary(op,x1,x2) -> Binary(op, loop bound x1, loop bound x2)
    | Apply(x1,x2) -> Apply(loop bound x1, loop bound x2)
    | Constant _ | Nonary _ as x -> x
    | Unary(op, x) -> Unary(op, loop bound x)
    | List_constructor xl -> List_constructor(List.map (fun x -> loop bound x) xl)
    | Set_constructor xl -> Set_constructor(List.map (fun x -> loop bound x) xl)
    | Formula_constructor f -> Formula_constructor(Formula.map (fun x -> loop bound x) f)
    | Lambda(n,x) -> Lambda(n, loop (SS.add n bound) x)
  in
  loop bound expression
;;
(* ***)
(*** eval_expression *)
let rec eval_expression it env x =
  tick it;
  match x with
  | Constant r -> r
  | Nonary b ->
      let f = Nonary_library.resolve it b in
      f ()
  | Unary(b, x) ->
      let f = Unary_library.resolve it b in
      let y = eval_expression it env x in
      f y
  | Binary(b, x1, x2) ->
      let f = Binary_library.resolve it b in
      let y1 = eval_expression it env x1 in
      let y2 = eval_expression it env x2 in
      f y1 y2
  | Ternary(b, x1, x2, x3) ->
      let f = Ternary_library.resolve it b in
      let y1 = eval_expression it env x1 in
      let y2 = eval_expression it env x2 in
      let y3 = eval_expression it env x3 in
      f y1 y2 y3
  | Apply(x1,x2) ->
      let y1 = eval_expression it env x1 in
      let y2 = eval_expression it env x2 in
      begin match y1 with
        | Thunk f -> f y2
        | _ -> raise (Because ("Type error in function application", Type_error))
      end
  | Content(Variable n) ->
      begin
        try
          SM.find n env
        with
        | Not_found -> raise (Unbound_value n)
      end
  (*| Content(Index(x1, x2)) ->*)
  | Lambda(x,f) ->
      let f' = replace_free_variables_of_expression ~bound:(SS.singleton x) ~env ~expression:f in
      (Thunk(fun v ->
        let env' = SM.add x v env in
        eval_expression it env' f'))
  | List_constructor xl -> List(List.map (eval_expression it env) xl)
  | Formula_constructor f -> Formula(Formula.map (eval_expression it env) f)
  | Set_constructor xl ->
      let yl = List.map (eval_expression it env) xl in
      begin
        match yl with
        | [] -> Nothing
        | y1 :: _ ->
            match y1 with
            | Unit_id _ -> 
                Unit_set(List.fold_left
                  (fun set y -> Unit_set.add (unit_id_of_value y) set) Unit_set.empty yl)
            | Package_id _ -> 
                Package_set(List.fold_left
                  (fun set y -> Package_set.add (package_id_of_value y) set) Package_set.empty yl)
            | Source_id _ -> 
                Source_set(List.fold_left
                  (fun set y -> Source_set.add (source_id_of_value y) set) Source_set.empty yl)
            | Archive_id _ -> 
                Archive_set(List.fold_left
                  (fun set y -> Archive_set.add (archive_id_of_value y) set) Archive_set.empty yl)
            | Date _ -> 
                Date_set(List.fold_left
                  (fun set y -> Date_set.add (date_of_value y) set) Date_set.empty yl)
            | _ -> raise (Because ("Type error in set construction", Type_error))
      end
  | _ -> raise Not_implemented
;;
(* ***)
(*** display_value *)
let display_value it v =
  let db = it.db in
  let cd = it.cd in
  let oc = cd.cd_out_channel in
  let fp = cd.cd_print in

  let p_package_id oc p_id =
    let default_architecture =
      try
        Some(Architecture_index.search (get_architecture_index db) it.cx.ctx_default_architecture)
      with
      | Not_found -> None
    in
    Functions.scribe_package_from_id db cd oc ?default_architecture p_id
  in

  let p_set iter set scribe =
    let first = ref true in
    fp oc "{";
    begin
      try
        iter
          begin fun id ->
            tick_out it;
            if !first then first := false else fp oc ",";
            fp oc " %a" (scribe db cd) id
          end
          set;
        fp oc " }";
      with
      | Output_limit_exceeded -> fp oc ", ... }"
    end
  in
  let rec p_value oc v =
    tick_out it;
    match v with
    | Bool b -> fp oc "%b" b
    | String u -> fp oc "%S" u
    | Date j -> scribe_date cd oc j
    | Int i -> fp oc "%d" i
    | Float f -> fp oc "%g" f
    | Version v_n -> Functions.scribe_version_from_number db cd oc v_n
    | Unit_id u_id -> Functions.scribe_unit_from_id db cd oc u_id
    | Archive_id a_id -> Functions.scribe_archive_from_id db cd oc a_id
    | Package_id p_id -> p_package_id oc p_id
    | Source_id s_id -> Functions.scribe_source_from_id db cd oc s_id
    | Thunk _ -> fp oc "<thunk>"
    | List [] -> fp oc "[]"
    | Formula f -> fp oc "<< %a >>" (p_formula ~nervous:false) f
    | List(v1 :: vr) ->
        fp oc "[%a" p_value v1;
        begin
          try
            List.iter
              begin fun v ->
                tick_out it;
                fp oc ";%a" p_value v
              end
              vr
          with
          | Output_limit_exceeded -> fp oc "; ..."
        end;
        fp oc "]"
    | Nothing -> fp oc "{ }"
    | Archive_set ars -> p_set Archive_set.iter ars Functions.scribe_archive_from_id
    | Package_set ps ->  p_set Package_set.iter ps (fun _ cd oc p_id -> p_package_id oc p_id)
    | Unit_set us ->     p_set Unit_set.iter    us Functions.scribe_unit_from_id
    | Source_set ss ->   p_set Source_set.iter  ss Functions.scribe_source_from_id
    | Date_set ss ->     p_set Date_set.iter    ss (fun _ cd oc -> scribe_date cd oc)
    | Versioned spec ->
			begin
				match spec with
				| Unit_version (u_id, s) ->
        	begin
         	 fp oc "[. %a" (Functions.scribe_unit_from_id db cd) u_id;
         	 match s with
         	 | Sel_ANY -> fp oc " .]"
         	 | Sel_LT  v -> fp oc " (< %a) .]"  (Functions.scribe_version_from_number db cd) v
         	 | Sel_LEQ v -> fp oc " (<= %a) .]" (Functions.scribe_version_from_number db cd) v
         	 | Sel_EQ  v -> fp oc " (= %a) .]"  (Functions.scribe_version_from_number db cd) v
         	 | Sel_GEQ v -> fp oc " (>= %a) .]"  (Functions.scribe_version_from_number db cd) v
         	 | Sel_GT  v -> fp oc " (> %a) .]" (Functions.scribe_version_from_number db cd) v
        	end
				| Glob_pattern g -> fp oc "%s .]" g
			end
    | Diagnosis dg ->
        fp oc "<diagnosis:closure size %d, %d failures>"
          dg.dg_closure_size
          (Array.length dg.dg_failures)
    | Diagnosis_list dg ->
        fp oc "<diagnosis_list:closure size %d, %d failures>"
          dg.dg_closure_size
          (Array.length dg.dg_failures)
    | _ -> fp oc "<unprintable-value>"
  and p_formula ?(nervous=false) oc f =
    tick_out it;
    match f with
    | True|And[] -> fp oc "true"
    | False|Or[] -> fp oc "false"
    | And[f]|Or[f] -> p_formula ~nervous oc f
    | And(f1 :: fr) ->
        fp oc "%a " (p_formula ~nervous:true) f1;
        List.iter
          begin fun f ->
            fp oc "and %a" (p_formula ~nervous:true) f;
          end
          fr
    | Or(f1 :: fr) ->
        if nervous then fp oc "(";
        fp oc "%a " (p_formula ~nervous:false) f1;
        List.iter
          begin fun f ->
            fp oc "or %a" (p_formula ~nervous:false) f;
          end
          fr;
        if nervous then fp oc ")"
    | Not f ->
        fp oc "not ";
        fp oc "%a" (p_formula ~nervous:true) f
    | Atom v -> fp oc "%a" p_value v (* XXX *)
  in
  p_value oc v;
  fp oc "\n"
;;
(* ***)
(*** display_diagnosis *)
let display_diagnosis it dg =
  let db = it.db in
  let cd = it.cd in
  let oc = cd.cd_out_channel in
  let fp = cd.cd_print in
  fp oc "Diagnosis:\n";
  fp oc "  Conflicts: %d\n" dg.dg_conflicts;
  fp oc "  Disjunctions: %d\n" dg.dg_disjunctions;
  fp oc "  Dependencies: %d\n" dg.dg_dependencies;
  fp oc "  Failures (total %d):\n" (Array.length dg.dg_failures);
  let print_package oc p_id =
    Functions.scribe_package_from_id db cd oc p_id
  in
  let show_range spec = 
	begin
		match spec with
		| Unit_version (u_id, s) -> 
			begin
				fp oc "%a" (Functions.scribe_unit_from_id db cd) u_id;
    		match s with
    		| Sel_ANY -> ()
    		| Sel_LT  v -> fp oc " (< %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_LEQ v -> fp oc " (<= %a)" (Functions.scribe_version_from_number db cd) v
    		| Sel_EQ  v -> fp oc " (= %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_GEQ v -> fp oc " (>= %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_GT  v -> fp oc " (> %a)" (Functions.scribe_version_from_number db cd) v
			end
		| Glob_pattern g -> fp oc "%s" g
	end in
  Array.iter
    begin fun (p_id, rl) ->
      fp oc "    Package %a cannot be installed:\n" print_package p_id;
      List.iter
        begin function
          | Not_available p_id' -> fp oc "      %a is not available\n" print_package p_id'
          | Requested p_id' -> fp oc "      %a has been requested\n" print_package p_id'
          | Conflict(p_id',p_id'') -> fp oc "      %a and %a conflict\n" print_package p_id' print_package p_id''
          | Empty_disjunction(p_id', rl) ->
              fp oc "      %a depends on missing:\n" print_package p_id';
              List.iter
                begin fun r ->
                  fp oc "        - ";
                  show_range r;
                  fp oc "\n"
                end
                rl
          | Dependency(p_id', dl) ->
              fp oc "      %a depends on one of:\n" print_package p_id';
              List.iter
                begin fun p_id'' ->
                  fp oc "        - %a\n" print_package p_id''
                end
                dl
        end
        rl
    end
    dg.dg_failures
;;
(* ***)
(*** display_diagnosis *)
let display_diagnosis_list it dg =
  let db = it.db in
  let cd = it.cd in
  let oc = cd.cd_out_channel in
  let fp = cd.cd_print in
  fp oc "Diagnosis:\n";
  fp oc "  Conflicts: %d\n" dg.dg_conflicts;
  fp oc "  Disjunctions: %d\n" dg.dg_disjunctions;
  fp oc "  Dependencies: %d\n" dg.dg_dependencies;
  fp oc "  Failures (total %d):\n" (Array.length dg.dg_failures);
  let print_package oc p_id =
    Functions.scribe_package_from_id db cd oc p_id
  in
  let rec print_package_list oc p_ids =
		match p_ids with
		| [] -> fp oc ""
		| [h] -> print_package oc h
		| h::t -> 
				(print_package oc h; fp oc " and "; print_package_list oc t)
  in
  let show_range spec = 
	begin
		match spec with
		| Unit_version (u_id, s) -> 
			begin
				fp oc "%a" (Functions.scribe_unit_from_id db cd) u_id;
    		match s with
    		| Sel_ANY -> ()
    		| Sel_LT  v -> fp oc " (< %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_LEQ v -> fp oc " (<= %a)" (Functions.scribe_version_from_number db cd) v
    		| Sel_EQ  v -> fp oc " (= %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_GEQ v -> fp oc " (>= %a)"  (Functions.scribe_version_from_number db cd) v
    		| Sel_GT  v -> fp oc " (> %a)" (Functions.scribe_version_from_number db cd) v
			end
		| Glob_pattern g -> fp oc "%s" g
	end
  in
  Array.iter
    begin fun (p_ids, rl) ->
      fp oc "    Packages %a cannot be installed together:\n" print_package_list p_ids;
      List.iter
        begin function
          | Not_available p_id' -> fp oc "      %a is not available\n" print_package p_id'
          | Requested p_id' -> fp oc "      %a has been requested\n" print_package p_id'
          | Conflict(p_id',p_id'') -> fp oc "      %a and %a conflict\n" print_package p_id' print_package p_id''
          | Empty_disjunction(p_id', rl) ->
              fp oc "      %a depends on missing:\n" print_package p_id';
              List.iter
                begin fun r ->
                  fp oc "        - ";
                  show_range r;
                  fp oc "\n"
                end
                rl
          | Dependency(p_id', dl) ->
              fp oc "      %a depends on one of:\n" print_package p_id';
              List.iter
                begin fun p_id'' ->
                  fp oc "        - %a\n" print_package p_id''
                end
                dl
        end
        rl
    end
    dg.dg_failures
;;
(* ***)
(*** Directives *)
module Directives =
  struct
    (*** d_dump *)
    let d_dump it = function
      | [String fn; v] ->
          Util.with_output_to_file fn
            begin fun oc ->
              let cd = conduit_of_channel oc in
              let it' =
                { it with
                  cx =
                     { it.cx with 
                       ctx_output_limit = max_int;
                       ctx_interactive = false };
                  cd = cd }
              in
              reset_output_limit it';
              display_value it' v;
              cd.cd_flush oc;
							close_out oc
            end
      | _ -> raise (Because ("#dump: Filename expected as first argument", Type_error))
    ;;
    (* ***)
    (*** d_help *)
    let d_help it yl =
			if yl = [] then
				it.cd.cd_print it.cd.cd_out_channel "%s" Help.help_string
			else match (List.hd yl) with
				| String s -> begin
						if s = "functions" then
							it.cd.cd_print it.cd.cd_out_channel "%s" Help.help_functions_string
						else if s = "directives" then
							it.cd.cd_print it.cd.cd_out_channel "%s" Help.help_directives_string
						else
							it.cd.cd_print it.cd.cd_out_channel "%s" Help.help_string
					end
				| _ -> it.cd.cd_print it.cd.cd_out_channel "%s" Help.help_string
    ;;
    (* ***)
    (*** d_exit *)
    let d_exit _it _yl = raise Quit;;
    (* ***)
    (*** d_merge *)
    let d_merge it yl =
      match yl with
      | [] -> it.cd.cd_print it.cd.cd_out_channel "No waterways to merge.\n"
      | _ ->
        List.iter
          begin function
            | String ww ->
                it.cd.cd_print it.cd.cd_out_channel "Merging %S...\n%!" ww;
                let s = Waterway.specification_of_string ww in
                Waterway.merge it.db s
            | _ -> raise (Because ("#merge: Waterway expected", Type_error))
          end
          yl
    ;;
    (* ***)
    (*** d_show *)
    let d_show it yl =
      List.iter
        begin function
        | Diagnosis dg -> display_diagnosis it dg
        | Diagnosis_list dg -> display_diagnosis_list it dg
        | v -> display_value it v
        end
        yl
    ;;
    (* ***)
		(*** d_type *)
		let d_type it _yl =
  	let oc = it.cd.cd_out_channel in
  	let fp = it.cd.cd_print in
			match (get_liquid it.db) with
				None -> fp oc "undetermined"
			| Some Debian -> fp oc "debian"
			| Some RPM -> fp oc "rpm"
			| Some Pkgsrc -> fp oc "ps"
		;;
		(* ***)
		(*** d_abundance *)
		let d_abundance it yl =
  		let oc = it.cd.cd_out_channel in
  		let fp = it.cd.cd_print in
			List.iter
				begin function
				| Package_set p ->
					begin
						match Installability.abundance it.db p with
						| None -> fp oc "Abundance condition fulfilled.\n"
						| Some p_id ->
							begin
								fp oc "Abundance condition NOT fulfilled by package: ";
								Functions.scribe_package_from_id it.db it.cd oc p_id;
								fp oc "\n"
							end
					end
				| _ -> raise (Because ("#abundance: Package set expected", Type_error))
				end
				yl
		;;
		(* ***)
    (*** definitions *)
    let definitions = [
      "exit",             d_exit;
      "quit",             d_exit;
      "merge",            d_merge;
      "help",             d_help;
      "dump",             d_dump;
      "show",             d_show;
			"type",             d_type;
			"abundance",        d_abundance;
    ];;
    (* ***)
    (*** resolve *)
    let resolve it d =
      try
        List.assoc d definitions
      with
      | Not_found -> raise (Unknown_directive d)
    ;;
    (* ***)
  end
;;
(* ***)
(*** assign *)
let assign it env l y =
  match l with
  | Variable n -> it.env <- SM.add n y it.env
  | Index(x1,x2) ->
      let y1 = eval_expression it env x1
      and y2 = eval_expression it env x2
      in
      let i = int_of_value y2 in
      match y1 with
      | Array a ->
          if i < 0 or i > Array.length a then raise (Sorry(sf "Index %d out of bounds" i))
          else
            a.(i) <- y
      | _ -> raise Type_error
;;
(* ***)
(*** execute_statement *)
let execute_statement it = function
| Nop -> ()
| Assignment(l, x) ->
    if it.cx.ctx_modify_environment then
      begin
        let y = eval_expression it it.env x in
        assign it it.env l y
      end
    else
      raise Access_denied
| Directive(d,xl) ->
    let f = Directives.resolve it d in
    let yl = List.map (eval_expression it it.env) xl in
    f it yl
;;
(* ***)
(*** execute_query *)
let execute_query it q =
  reset_limits it;
  match q with
  | Expression x -> display_value it (eval_expression it it.env x)
  | Statement s -> execute_statement it s
;;
(* ***)
