(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt and the EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type t = {
  mutable year : int;
  mutable month : int; (* 1..12 *)
  mutable day : int; (* 1..31 *)
};;
type u = int * int * int;;

let copy t = { t with year = t.year };;

let to_string g = Printf.sprintf "%04d-%02d-%02d" g.year g.month g.day;;

let make (y,m,d) = { year = y; month = m; day = d };;
let unmake g = (g.year, g.month, g.day);;

let set g (y,m,d) = g.year <- y; g.month <- m; g.day <- d;;

let get g = (g.year, g.month, g.day);;

let ( *** ) x y =
  if x = 0 then
    y
  else
    x
;;

let compare g1 g2 = (compare g1.year g2.year) *** (compare g1.month g2.month) *** (compare g1.day g2.day)
;;

let is_year_leap y =
  (y mod 400 = 0) || ((y land 3 = 0) && not (y mod 100 = 0))
;;

let is_leap g =
  let y = g.year in
  (y mod 400 = 0) || ((y land 3 = 0) && not (y mod 100 = 0))
;;

let month_lengths =      [|31;28;31;30;31;30;30;31;30;31;30;31|]
let leap_month_lengths = [|31;29;31;30;31;30;30;31;30;31;30;31|]

let month_length y m =
  let ml = if is_year_leap y then leap_month_lengths else month_lengths in
  ml.(m - 1)
;;

let increment g =
  let ml = if is_leap g then leap_month_lengths else month_lengths in
  g.day <- g.day + 1;
  if g.day > ml.(g.month - 1) then
    begin
      g.day <- 1;
      g.month <- g.month + 1;
      if g.month > 12 then
        begin
          g.year <- g.year + 1;
          g.month <- 1;
          g.day <- 1
        end
    end
;;

let decrement g =
  g.day <- g.day - 1;
  if g.day = 0 then
    begin
      g.month <- g.month - 1;
      if g.month = 0 then
        begin
          g.year <- g.year - 1;
          g.month <- 12;
          g.day <- 31
        end
      else
        let ml = if is_leap g then leap_month_lengths else month_lengths in
        g.day <- ml.(g.month - 1)
    end
;;

let today () =
  let tm = Unix.gmtime (Unix.gettimeofday ()) in
  make (tm.Unix.tm_year + 1900, tm.Unix.tm_mon + 1, tm.Unix.tm_mday)
;;

let iter start stop f =
  let lo = make start
  and hi = make stop
  in
  while compare lo hi <= 0 do
    f (get lo);
    increment lo
  done
;;
