(* Copyright 2005-2007 Berke DURAK, Inria Rocquencourt and the EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type t = { mutable year : int; mutable month : int; mutable day : int; }
type u = int * int * int
val to_string : t -> string
val make : u -> t
val unmake : t -> int * int * int
val set : t -> u -> unit
val get : t -> u
val ( *** ) : int -> int -> int
val compare : t -> t -> int
val is_leap : t -> bool
val month_lengths : int array
val month_length : int -> int -> int
val leap_month_lengths : int array
val increment : t -> unit
val decrement : t -> unit
val copy : t -> t
val today : unit -> t
val iter : u -> u -> (u -> unit) -> unit
