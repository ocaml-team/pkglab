(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, the EDOS Project and
Jaap Boender.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

%{
(* Prolog *)
open Query
open Formula
let str s = Constant(String s);;
%}
%token EOF
%token AND
%token ARCHIVES
%token ASSIGN
%token AT
%token BEGIN
%token CHECK
%token CHECK_TOGETHER
%token CLOSURE
%token COLON
%token COMMA
%token CONFLICT_LIST
%token CONFLICTS
%token CONTENTS
%token COUNT
%token DEP_CLOSURE
%token DEP_PATH
%token DEPENDS
%token DIFF
%token DIFFICULTY
%token DOT
%token ELEMENTS
%token ENCODE
%token END
%token EQ
%token EXISTS
%token FALSE
%token FILTER
%token FORALL
%token FROM_SOURCE
%token FROM_UNIT
%token GEQ
%token GT
%token IN
%token INSTALL
%token INSTALLED_SIZE
%token IS_BUILD_ESSENTIAL
%token IS_EMPTY
%token IS_ESSENTIAL
%token LANGLE
%token LATEST
%token LBRACE
%token LBRACK
%token LBRACKDOT
%token LEQ
%token LET
%token LOAD_PACKAGES
%token LPAREN
%token LT
%token MAP
%token MAPTO
%token MEMBER
%token NEQ
%token NOT
%token OR
%token PACKAGES
%token PRE_CLOSURE
%token PRE_DEPENDS
%token PROVIDES
%token RANGE
%token RANGLE
%token RBRACE
%token RBRACK
%token RDOTBRACK
%token REPLACES
%token REQUIRES
%token RPAREN
%token SELECT
%token SEMICOLON
%token SINGLETON
%token SIZE
%token SOLVE
%token SOURCE
%token SOURCES
%token STRONG_DEP
%token SUCCESS
%token TEMPERATURE
%token TILDE
%token TREAT
%token TRIM
%token TRUE
%token UNIT
%token UNITS
%token VERSIONS
%token WHAT_PROVIDES

%token <int> INT
%token <int * int * int> DATE
%token <string> ARCHIVE_NAME
%token <string> VERSION_NAME
%token <string> SOURCE_VERSION_NAME
%token <string> ARCHITECTURE
%token <string> DIRECTIVE
%token <string> IDENT
%token <string> UNIT_NAME
%token <string> STRING
%token <string * [`Word_boundary|`Case_insensitive] list> REGEXP
%type <Query.expression> expression
%type <Query.query> query
%type <Query.program> program
%start expression
%start query
%start program

/* Priorities */
%nonassoc APPLY
%nonassoc IDENT
%nonassoc LBRACK
%left DOT
%left TILDE
%nonassoc WHAT_PROVIDES
%nonassoc VERSIONS
%left SEMICOLON
%left OR
%left AND
%left DIFF

%nonassoc NOT
%left NEQ
%left LEQ
%left GEQ
%left EQ
%left GT
%left LT
%nonassoc IN
%right MAPTO

%nonassoc LETPREC

%%
program :
| statement EOF { [$1] }
| statement SEMICOLON program { $1::$3 }

statement :
| location ASSIGN expression { Assignment($1,$3) }
| DIRECTIVE { Directive($1,[]) }
| DIRECTIVE simple_expression_list { Directive($1, List.rev $2) }
| { Nop }

query :
| expression EOF { Expression($1) }
| statement EOF { Statement($1) }

(*prefix_ternary :
| RANGE           { Range        }*)
prefix_ternary :
| STRONG_DEP      { Strong_dep     }
| DEP_PATH        { Dep_path       }

prefix_binary :
| CHECK           { Check          }
| CHECK_TOGETHER  { Check_together }
| CONTENTS        { Contents       }
| DIFFICULTY      { Difficulty     }
| ENCODE          { Encode         }
| EXISTS          { Exists         }
| FILTER          { Filter         }
| FORALL          { Forall         }
| INSTALL         { Install        }
| MEMBER          { Member         }
| TREAT           { Treat          }
| MAP             { Map            }

prefix_unary :
| CLOSURE             { Closure              }
| CONFLICT_LIST       { Conflict_list        }
| CONFLICTS           { Conflicts            }
| COUNT               { Count                }
| DEP_CLOSURE         { Dep_closure          }
| DEPENDS             { Depends              }
| ELEMENTS            { Elements       }
| FROM_SOURCE         { From_source          }
| FROM_UNIT           { From_unit            }
| INSTALLED_SIZE      { Installed_size       }
| IS_BUILD_ESSENTIAL  { Is_build_essential   }
| IS_EMPTY            { Is_empty             }
| IS_ESSENTIAL        { Is_essential         }
| LATEST              { Latest               }
| LOAD_PACKAGES       { Load_packages        }
| PRE_CLOSURE         { Pre_closure          }
| PRE_DEPENDS         { Pre_depends          }
| PROVIDES            { Provides             }
| RANGE               { Range                }
| REPLACES            { Replaces             }
| REQUIRES            { Requires             }
| SELECT              { Select               }
| SINGLETON           { Singleton            }
| SIZE                { Size                 }
| SOURCE              { Source               }
| SUCCESS							{ Success              }
| TEMPERATURE         { Temperature          }
| TRIM                { Trim                 }
| UNIT                { Unit                 }
| VERSIONS            { Versions             }
| WHAT_PROVIDES       { What_provides        }

nonary :
| ARCHIVES        { Archives       }
| PACKAGES        { Packages       }
| SOURCES         { Sources        }
| UNITS           { Units          }

simple_expression_list :
| simple_expression { [ $1 ] }
| simple_expression_list simple_expression { $2::$1 }

booleable_expression_list :
| booleable_expression { [ $1 ] }
| booleable_expression_list booleable_expression { $2::$1 }

list_constructor :
| expression SEMICOLON list_constructor { $1::$3 }
| expression { [$1] }

set_constructor :
| expression COMMA set_constructor { $1::$3 }
| expression { [$1] }

location :
| IDENT { Variable $1 }
| simple_expression DOT LBRACK expression RBRACK { Index($1,$4) }

formula :
| simple_expression { Atom $1 }
| formula AND formula { And[$1; $3] }
| formula OR formula { Or[$1; $3] }
| NOT formula { Not $2 }
| TRUE { True }
| FALSE { False }
| LPAREN formula RPAREN { $2 }
| LANGLE booleable_expression RANGLE { Atom $2 }

booleable_expression :
| NOT simple_expression { Unary(Complement, $2) }
| LPAREN expression RPAREN { $2 }
| simple_expression AND expression { Binary(Intersection, $1, $3) }
| simple_expression OR expression { Binary(Union, $1, $3) }
| simple_expression { $1 }

simple_expression :
| TRUE { Constant(Bool true) }
| FALSE { Constant(Bool false) }
| IDENT { Content(Variable $1) }
| simple_expression DOT LBRACK expression RBRACK { Content(Index($1,$4)) }
| simple_expression TILDE simple_expression { Binary(Match,$3,$1) }
| nonary { Nonary $1 }
| simple_expression LT expression { Binary(Comparison Lt,$1,$3) }
| simple_expression LEQ expression { Binary(Comparison Le,$1,$3) }
| simple_expression EQ expression { Binary(Comparison Eq,$1,$3) }
| simple_expression GEQ expression { Binary(Comparison Ge,$1,$3) }
| simple_expression GT expression { Binary(Comparison Gt,$1,$3) }
| simple_expression NEQ expression { Binary(Comparison Ne,$1,$3) }
| simple_expression DIFF expression { Binary(Diff,$1,$3) }
| IDENT MAPTO expression { Lambda($1, $3) }
| LBRACKDOT expression RDOTBRACK { Ternary(Build_versioned, $2, Constant Nothing, Constant Nothing) }
| LBRACKDOT u = simple_expression LPAREN LT v = simple_expression RPAREN RDOTBRACK
  { Ternary(Build_versioned, u, str "<", v) }
| LBRACKDOT u = simple_expression LPAREN LEQ v = simple_expression RPAREN RDOTBRACK
  { Ternary(Build_versioned, u, str "<=", v) }
| LBRACKDOT u = simple_expression LPAREN EQ v = simple_expression RPAREN RDOTBRACK
  { Ternary(Build_versioned, u, str "=", v) }
| LBRACKDOT u = simple_expression LPAREN GEQ v = simple_expression RPAREN RDOTBRACK
  { Ternary(Build_versioned, u, str ">=", v) }
| LBRACKDOT u = simple_expression LPAREN GT v = simple_expression RPAREN RDOTBRACK
  { Ternary(Build_versioned, u, str ">", v) }
| LBRACK list_constructor RBRACK { List_constructor $2 }
| LBRACK RBRACK { List_constructor[] }
| LBRACE set_constructor RBRACE { Set_constructor $2 }
| LBRACE RBRACE { Set_constructor[] }
| LANGLE formula RANGLE { Formula_constructor $2 }
(* | BEGIN expression END { $2 } *)
| LPAREN expression RPAREN { $2 }
| INT { Constant(Int $1) };;
| STRING { str $1 }
| DATE { Constant(Date(Lifetime.day_of_ymd $1)) }
| ARCHIVE_NAME { Unary(Find_archive, str $1) }
| UNIT_NAME { Unary(Find_unit, str $1) }
| UNIT_NAME VERSION_NAME ARCHITECTURE { Ternary(Find_package, str $1, str $2, str $3) }
| UNIT_NAME VERSION_NAME { Ternary(Find_package, str $1, str $2, Constant Nothing) }
| UNIT_NAME SOURCE_VERSION_NAME { Binary(Find_source, str $1, str $2) }
| VERSION_NAME { Unary(Find_version, str $1) }
| prefix_unary LPAREN expression RPAREN { Unary($1, $3) }
| REGEXP { Constant(Regexp
  begin
    let (r,o) = $1 in
    let r =
      if List.mem `Word_boundary o then
        "\\b"^r^"\\b"
      else
        r
    in
    let flags =
      if List.mem `Case_insensitive o then
        [`CASELESS]
      else
        []
    in
    Pcre.regexp ~study:true ~flags r
  end) }

expression :
| LET IDENT EQ expression IN expression %prec LETPREC { Apply(Lambda($2,$6),$4) } 
| booleable_expression { $1 }
| booleable_expression booleable_expression_list %prec APPLY {let l = List.rev $2 in List.fold_left (fun res f -> Apply(res,f)) $1 l }
| prefix_ternary LPAREN expression COMMA expression COMMA expression RPAREN { Ternary($1,$3,$5,$7) }
| prefix_binary LPAREN expression COMMA expression RPAREN { Binary($1,$3,$5) }
