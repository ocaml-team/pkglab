(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

{
open Syntax

exception Parse_error of int * int * string

let lexical_error l s = raise (Parse_error(Lexing.lexeme_start l, Lexing.lexeme_end l,s))

let keywords = [
		"archives", ARCHIVES;
		"and", AND;
		"begin", BEGIN;
		"check", CHECK;
		"check_together", CHECK_TOGETHER;
		"closure", CLOSURE;
		"conflict_list", CONFLICT_LIST;
		"conflicts", CONFLICTS;
		"contents", CONTENTS;
		"count", COUNT;
		"depends", DEPENDS;
		"dep_closure", DEP_CLOSURE;
		"dep_path", DEP_PATH;
		"difficulty", DIFFICULTY;
		"elements", ELEMENTS;
		"end", END;
		"encode", ENCODE;
		"exists", EXISTS;
		"false", FALSE;
		"filter", FILTER;
		"forall", FORALL;
		"from_source", FROM_SOURCE;
		"from_unit", FROM_UNIT;
		"strong_dep", STRONG_DEP;
		"in", IN;
		"install", INSTALL;
		"is_essential", IS_ESSENTIAL;
		"is_empty", IS_EMPTY;
		"is_build_essential", IS_BUILD_ESSENTIAL;
		"in", IN;
		"installed_size", INSTALLED_SIZE;
		"latest", LATEST;
		"let", LET;
		"map", MAP;
		"member", MEMBER;
		"not", NOT;
		"or", OR;
		"packages", PACKAGES;
		"pre_depends", PRE_DEPENDS;
		"pre_closure", PRE_CLOSURE;
		"load_packages", LOAD_PACKAGES;
		"provides", PROVIDES;
		"range", RANGE;
		"replaces", REPLACES;
		"requires", REQUIRES;
		"select", SELECT;
		"singleton", SINGLETON;
		"size", SIZE;
		"solve", SOLVE;
		"source", SOURCE;
		"sources", SOURCES;
		"success", SUCCESS;
		"temperature", TEMPERATURE;
		"treat", TREAT;
		"trim", TRIM;
		"true", TRUE;
		"unit", UNIT;
		"units", UNITS;
		"versions", VERSIONS;
		"what_provides", WHAT_PROVIDES;
];;

let keywords_table =
  let h = Hashtbl.create 64 in
  List.iter (fun (kwd,tok) -> Hashtbl.add h kwd tok) keywords;
  h
;;
}
let blancs = [' ' '\n' '\t' '\r']+
let upperalpha = ['A'-'Z']
let alpha = ['a'-'z''A'-'Z']
let alphanum = alpha | ['0'-'9']
let decimaldigit = ['0'-'9']
let sign = '+'|'-'
let space = [' ''\t''\r''\n']
rule token = parse
(* Operators *)
(* Various non-alphabetic symbols *)
| "(" { LPAREN }
| ")" { RPAREN }
| "[." { LBRACKDOT }
| ".]" { RDOTBRACK }
| "[" { LBRACK }
| "]" { RBRACK }
| "{" { LBRACE }
| "}" { RBRACE }
| "<<" { LANGLE }
| ">>" { RANGLE }
| "," { COMMA }
| "," { COMMA }
(*| "+" { PLUS }*)
(*| "*" { STAR }*)
| "~" { TILDE }
| ":" { COLON }
| ";" { SEMICOLON }
| "<-" { ASSIGN }
| "->" { MAPTO }
| "<>" { NEQ } 
| "!=" { NEQ }
| "=" { EQ }
| "<=" { LEQ }
| ">=" { GEQ }
| "<" { LT }
| ">" { GT }
| "," { COMMA }
(*| "=~" { MATCHES }
| "!~" { DOESNT_MATCH }*)
| "&" { AND }
| "|" { OR }
| "!" { NOT }
| "\\" { DIFF }
| "@" { AT }
| "#" ((alphanum|'_')+ as w) { DIRECTIVE w }
| "'" ((alphanum | '.'|'+'|'-'|':'|'~'|'*'|'['|']'|'?')+ as w) { VERSION_NAME w }
| "@" (alphanum+ as a) { ARCHITECTURE a }
| "`" ((alphanum | '.'|'+'|'-'|':'|'~'|'*'|'['|']'|'?')+ as w) { SOURCE_VERSION_NAME w }
| "\"" { STRING(readstr lexbuf) }
| "/" { REGEXP(readregexp lexbuf) }
| "$"((alphanum|'_')+ as w) { IDENT w }
| decimaldigit+ as w { INT(int_of_string w) }
| (['0'-'9'] ['0'-'9'] ['0'-'9'] ['0'-'9'] as y) '-' (['0'-'9'] ['0'-'9'] as m) '-' (['0'-'9'] ['0'-'9'] as d)
  { DATE(int_of_string y, int_of_string m, int_of_string d) }
| '_'((alphanum|'_'|'-'|'.'|'+'|':'|'/')+ as w) { UNIT_NAME w }
| ['a'-'z']['a'-'z' '_']*['a'-'z'] as w {
  try
    Hashtbl.find keywords_table w
  with
  | Not_found -> UNIT_NAME w }
| (alphanum|'_'|'-'|'.'|'+'|':')+ as w { UNIT_NAME w }
| "{"((alphanum|'_'|'-'|'.'|'+'|':'|'('|')'|'/')+ as w)"}" { UNIT_NAME w }
| '%'((alphanum|'_'|'-'|'.'|'+'|':'|'/')+ as w) { ARCHIVE_NAME w }
(* Comments, space, strings and end of file *)
| "(*" { eat_comment lexbuf; token lexbuf }
| space+ { token lexbuf }
| eof { EOF }
| _ as c { lexical_error lexbuf (Printf.sprintf "Unexpected %C" c) }
and eat_comment = parse
  "(*" { eat_comment lexbuf; eat_comment lexbuf }
| "*)" { }
| [^'(' '*']+ { eat_comment lexbuf }
| _ { eat_comment lexbuf }
and readstr = parse
  "\"" { "" }
| "\\\"" { "\""^(readstr lexbuf) }
| [^'"' '\\']+ { let s = Lexing.lexeme lexbuf in s^(readstr lexbuf) }
and readregexp = parse
  "/" { ("",readregexpoptions lexbuf) }
| "\\/" { let (s,o) = readregexp lexbuf in ("/"^s,o) }
| ([^'/' '\\']+ | '\\'[^'/'][^'/' '\\']*) as s { let (s',o) = readregexp lexbuf in (s^s',o) }
and readregexpoptions = parse
 (['w' 'i']* as w) {
    let rec loop r i =
      if i = String.length w then
        r
      else
        loop
          ((match w.[i] with
             'w' -> `Word_boundary
           | 'i' -> `Case_insensitive
           | _ -> assert false)::r)
          (i + 1)
    in
    loop [] 0 }
and readident = parse
  alpha (alphanum|'_')* as id { id }
{
(* Epilogue. *)
}
