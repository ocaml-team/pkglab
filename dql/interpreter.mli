(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)
(** This module interpets queries in the DQL language, given a [Rapids] database. *)

(** The exception [Parse_error(i,j,msg)] is raised when [parse_query] or [parse_expression]
    have been called with a string [u] having a syntax error described by [msg] and identified
    to be between positions [i] and [j] where [0 <= i <= j <= String.length u]. *)
exception Parse_error of int * int * string

exception Unbound_value of string
exception Unknown_directive of string
exception Sorry of string
exception Type_error
exception Because of string * exn
exception Time_limit_exceeded
exception Space_limit_exceeded
exception Output_limit_exceeded
exception Access_denied
exception Not_implemented
exception Quit

(** The interpreter can run in a variety of environments: as an interactive command-line tool,
    in batch mode, as a backend for web server, in various scripts, and so on.

    Each such environment has its own specific requirements.  For instance, in batch mode,
    the interpreter must not ask any interactive questions.  In web mode, it should not
    print anything on stdout or stderr; it should not allow access or modification of local
    files.  And so on.

    A [context] is a record aggregating information on how the queries interact with the environment.
*)

type context = {
  ctx_space_limit : int;             (** Memory limit for evaluating expressions *)
  ctx_time_limit : int;              (** Time limit for evaluating expressions *)
  ctx_output_limit : int;            (** Output limit *)
  ctx_filesystem_access : bool;      (** Access to local files allowed? *)
  ctx_modify_database : bool;        (** Can the Rapids database be modified? *)
  ctx_modify_environment : bool;     (** Can modify the toplevel environment *)
  ctx_interactive : bool;            (** Can the interpreter do interactive prompts? *)
  ctx_default_architecture : string; (** Default architecture *)
}

(** Default context for command-line interactions *)
val default_cli_context : ?architecture:string -> unit -> context

(** Default context for unauthenticated web interactions *)
val default_web_context : ?architecture:string -> unit -> context

(** Default context for batch processing *)
val default_batch_context :  ?architecture:string -> unit -> context

(** An interpreter. *)
type 'a t

(** The type of variable environments *)
type env

(** The default environment *)
val env0 : env

(** Given a [Rapids] database and a context, create an interpreter. *)
val create_interpreter : Rapids.db -> context -> 'a Conduit.conduit -> 'a t

(** Parse the given DQL expression.  Thread-safe & reentrant. *)
val parse_expression : string -> Query.expression

(** Parse the given DQL query.  Thread-safe & reentrant. *)
val parse_query : string -> Query.query

(** Parse the given DQL program.  Thread-safe & reentrant. *)
val parse_program : string -> Query.program

(** Evaluate the given expression inside the interpreter.  NOT reentrant. WON'T modify the database. *)
val eval_expression : 'a t -> env -> Query.expression -> Query.value_

(** Evaluate the given statement inside the interpreter.  NOT reentrant. MAY modify the database or
    access the filesystem depending on the context used to create the interpreter. *)
val execute_statement : 'a t -> Query.statement -> unit

(** Evaluate the given query. *)
val execute_query : 'a t -> Query.query -> unit
