(* Copyright 2005-2007 Berke DURAK, Inria Rocquencourt and the EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

let sf = Printf.sprintf;;

(*** put_arrows *)
let put_arrows out i j =
  for h = 1 to i do
    out " "
  done;
  for h = i to j do
    out "^";
  done;
  out "\n"
;;
(* ***)
(*** show_highlighted *)
let show_highlighted out w i j n =
  let m = String.length w in
  let j = max 0 (min (m - 1) j) in
  let b = min (n / 3) (j - i + 1) in
  let ps = min (m - b) (n - b) in
  let s = min (m - j - 1) ((ps + 1) / 2) in
  let p = min i (ps - s) in
  let s = min (m - j - 1) (ps - p) in
  let p_i = i - p in
  let hi_i =
    if p_i > 0 then
      begin
        out "...";
        out (String.sub w p_i p);
        p + 3
      end
    else
      begin
        out (String.sub w 0 p);
        p
      end
  in
  if b < j - i + 1 then
    begin
      let b' = b - 3 in
      let bl = b' / 2 in
      let br = b' - bl
      in
      out (String.sub w i bl);
      out "...";
      out (String.sub w (j - br) br)
    end
  else
    out (String.sub w i b);
  if j + 1 + s < m then
    begin
      out (String.sub w (j + 1) s);
      out "..."
    end
  else
    out (String.sub w (j + 1) s);
  out "\n";
  put_arrows out hi_i (hi_i + b - 1)
;;
(* ***)
(*** escape_and_record_indexes *)
let escape_and_record_indexes w l =
  let m = String.length w in
  let b = Buffer.create m in
  let r = ref [] in
  for i = 0 to m - 1 do
    if List.mem i l then
      r := (i,Buffer.length b)::!r;
    Buffer.add_string b (String.escaped (String.make 1 w.[i]))
  done;
  if List.mem m l then
    r := (m,Buffer.length b)::!r;
  (Buffer.contents b,!r)
;;
(* ***)

let lower_half x = x / 2
let upper_half x = x - (x / 2)

(*** show_parse_error *)
let show_parse_error ~out ?(columns=75) i j x w =
  let m = String.length w in
  if m = 0 then
    out "Error: Syntax error -- Empty query.\n"
  else
    begin
        out (sf "Error: Syntax error %s of query --- %s:\n"
          (if i = j then 
            if i >= m - 1 then
              "end"
            else
              sf "at character %d" (i + 1)
           else "between "^
           (if i = 0 then "beginning" else sf "character %d" (i + 1))^
           " and "^
           (if j >= m - 1 then "end" else sf "character %d" (j + 1)))
          x);
        let (w',z) = escape_and_record_indexes w [i;j] in
        let m = String.length w'
        and i' = List.assoc i z
        and j' = List.assoc j z
        in
        (* show string w' highlighting i' to j' on columns columns *)
        let w' = if j' >= m - 1 then w'^" " else w' in
        show_highlighted out w' i' j' columns
      end
;;
(* ***)
