(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type tidbit =
	Simple of string
|	Bold of string
| Underscore of string
| Red of string
| Green of string
| Brown of string
| Blue of string
| Magenta of string
| Cyan of string
| White of string
| Tab
| Newline

type colourful = tidbit list;;

let tidbit_to_string (t: tidbit): string =
begin
	match t with
	| Simple s -> s
	| Bold s -> Format.sprintf "\x1b[1m%s\x1b[0m" s
	| Underscore s -> Format.sprintf "\x1b[4m%s\x1b[0m" s
	| Red s -> Format.sprintf "\x1b[31m%s\x1b[0m" s
	| Green s -> Format.sprintf "\x1b[32m%s\x1b[0m" s
	| Brown s -> Format.sprintf "\x1b[33m%s\x1b[0m" s
	| Blue s -> Format.sprintf "\x1b[34m%s\x1b[0m" s
	| Magenta s -> Format.sprintf "\x1b[35m%s\x1b[0m" s
	| Cyan s -> Format.sprintf "\x1b[36m%s\x1b[0m" s
	| White s -> Format.sprintf "\x1b[37m%s\x1b[0m" s
	| Tab -> "\t"
	| Newline -> "\n"
end;;

let colourful_to_string (str: colourful): string =
	String.concat "" (List.map tidbit_to_string str);;

let help_string =
	colourful_to_string [
		Simple "Basic entities and operators:"; Newline;
		Simple "  \""; Red "string"; Simple "\""; Tab; Tab; Tab; Simple "String (filename, waterway)"; Newline;
		Simple "  "; Red "unit"; Simple "'"; Red "version"; Simple "@"; Red "arch"; Tab; Tab; Simple "Package (e.g. kde'5:47@all)"; Newline;
		Simple "  "; Red "unit"; Simple "`"; Red "version"; Tab; Tab; Tab; Simple "Source package"; Newline;
		Simple "  %"; Red "name"; Tab; Tab; Tab; Tab; Simple "Archive"; Newline;
		Simple "  $"; Red "name"; Tab; Tab; Tab; Tab; Simple "Variable"; Newline;
		Simple "  "; Red "yyyy"; Simple "-"; Red "mm"; Simple "-"; Red "dd"; Tab; Tab; Tab; Simple "Date"; Newline;
		Simple "  ["; Red "date1"; Simple ";"; Red "date2"; Simple "]"; Tab; Tab; Tab; Simple "Date range"; Newline;
		Simple "  [."; Red "unit"; Simple "("; Red "operator version"; Simple ").]"; Tab; Simple "Version specification (e.g. [.kde (>= 5.2).])"; Newline;
		Tab; Tab; Tab; Tab; Simple "(version part is optional)"; Newline;
		Simple "  {"; Red "e1"; Simple ","; Red "e2"; Simple ","; Red "..."; Simple ","; Red "en"; Simple "}"; Tab; Tab; Simple "Set construction"; Newline;
		Simple "  "; Red "set1"; Simple "|"; Red "set2"; Simple " "; Red "set1"; Simple "&"; Red "set2"; Simple " "; Red "set1"; Simple "\\"; Red "set2"; Tab; Simple "Set union/intersection/difference"; Newline;
		Simple "  not "; Red "bool"; Simple "  "; Red "b1"; Simple " and "; Red "b2"; Simple "  "; Red "b1"; Simple " or "; Red "b2"; Tab; Simple "Boolean operators"; Newline;
		Simple "  "; Red "x"; Simple "="; Red "y"; Simple " "; Red "x"; Simple "<>"; Red "y"; Simple " "; Red "x"; Simple ">"; Red "y"; Simple " "; Red "x"; Simple "<"; Red "y"; Simple " "; Red "x"; Simple ">="; Red "y"; Simple " "; Red "x"; Simple "<="; Red "y"; Tab; Simple "Comparison (if applicable; also for sets)"; Newline;
		Simple "  "; Red "set"; Simple " ~ /"; Red "regexp"; Simple "/"; Tab; Tab; Simple "Regular expression matching"; Newline;
		Simple "  "; Red "packages"; Tab; Tab; Tab; Simple "Show all packages"; Newline;
		Simple "  "; Red "archives"; Tab; Tab; Tab; Simple "Show all archives"; Newline;
		Simple "  "; Red "sources"; Tab; Tab; Tab; Simple "Show all source packages"; Newline;
		Simple "  "; Red "units"; Tab; Tab; Tab; Tab; Simple "Show all units"; Newline;
		Simple "Other help sections:"; Newline;
		Simple "  #help \"functions\""; Tab; Tab; Simple "Functions"; Newline;
		Simple "  #help \"directives\""; Tab; Tab; Simple "Directives"; Newline
		];;

let help_functions_string =
	colourful_to_string [
		Simple "Functions:"; Newline;
		Simple "  check("; Red "set1"; Simple ","; Red "set2"; Simple ")"; Tab; Tab; Simple "Check whether "; Underscore "set1"; Simple " is installable using "; Underscore "set2"; Newline;
		Simple "  check_together("; Red "set1"; Simple ","; Red "set2"; Simple ")"; Tab; Simple "Check if "; Underscore "set1"; Simple " is installable together using "; Underscore "set2"; Newline;
		Simple "  closure("; Red "set"; Simple ")"; Tab; Tab; Tab; Simple "Dependency cone of "; Underscore "set"; Simple " (depends and pre-depends)"; Newline; 
		Simple "  conflict_list("; Red "package"; Simple ")"; Tab; Simple "Conflict specification of "; Underscore "package"; Newline;
		Simple "  conflicts("; Red "set"; Simple ")"; Tab; Tab; Simple "Packages that conflict with packages in "; Underscore "set";  Newline;
		Simple "  contents("; Red "archive"; Simple ","; Red "date"; Simple ")"; Tab; Simple "Contents of "; Underscore "archive"; Simple " on "; Underscore "date"; Newline;
		Simple "  count("; Red "set"; Simple ")"; Tab; Tab; Tab; Simple "Number of elements of "; Underscore "set"; Newline;
		Simple "  dep_closure("; Red "set"; Simple ")"; Tab; Tab; Simple "Dependency cone of "; Underscore "set"; Simple " (depends only)"; Newline;
    Simple "  dep_path("; Red "set"; Simple ","; Red "pkg1"; Simple ","; Red "pkg2"; Simple ")"; Tab; Simple "Dependency path in "; Underscore "set"; Simple " from "; Underscore "pkg1"; Simple " to "; Underscore "pkg2"; Newline;
		Simple "  depends("; Red "package"; Simple ")"; Tab; Tab; Simple "Dependency specification of "; Underscore "package"; Newline;
    Simple "  elements("; Red "set"; Simple ")"; Tab; Tab; Tab; Simple "Returns a list containing the elements of "; Underscore "set"; Newline;
		Simple "  exists("; Red "set"; Simple ","; Red "function"; Simple ")"; Tab; Tab; Underscore "function"; Simple " is true for any element of "; Underscore "set"; Newline;
		Simple "  forall("; Red "set"; Simple ","; Red "function"; Simple ")"; Tab; Tab; Underscore "function"; Simple " is true for all elements of "; Underscore "set"; Newline;
		Simple "  filter("; Red "set"; Simple ","; Red "function"; Simple ")"; Tab; Tab; Simple "Filter "; Underscore "set"; Simple " by "; Underscore "function"; Newline;
		Simple "  install("; Red "set1"; Simple ","; Red "set2"; Simple ")"; Tab; Tab; Simple "Give a possible installation of "; Underscore "set1"; Simple " in "; Underscore "set2"; Newline;
    Simple "  installed_size("; Red "package"; Simple ")"; Tab; Simple "Installed size of "; Underscore "package"; Newline;
    Simple "  is_empty("; Red "set"; Simple ")"; Tab; Tab; Tab; Simple "Tests whether "; Underscore "set"; Simple " is empty"; Newline;
		Simple "  load_packages("; Red "filename"; Simple ")"; Tab; Simple "Load packages from file "; Underscore "filename"; Newline;
		Simple "  map("; Red "set"; Simple ","; Red "function"; Simple ")"; Tab; Tab; Simple "Map "; Underscore "set"; Simple " by "; Underscore "function"; Newline;
		Simple "  member("; Red "elt"; Simple ","; Red "set"; Simple ")"; Tab; Tab; Underscore "set"; Simple " contains "; Underscore "elt"; Newline;
		Simple "  pre_closure("; Red "set"; Simple ")"; Tab; Tab; Simple "Pre-dependency cone of "; Underscore "set"; Newline;
		Simple "  provides("; Red "package"; Simple ")"; Tab; Tab; Simple "Units provided by "; Underscore "package"; Newline;
		Simple "  range("; Red "archive"; Simple ")"; Tab; Tab; Simple "Range of dates available in "; Underscore "archive"; Newline;
		Simple "  select("; Red "specification"; Simple ")"; Tab; Tab; Simple "All packages satisfying a version specification"; Newline;
    Simple "  size("; Red "package"; Simple ")"; Tab; Tab; Tab; Simple "Package size of "; Underscore "package"; Newline;
    Simple "  source("; Red "package"; Simple ")"; Tab; Tab; Simple "Source package of "; Underscore "package"; Newline;
		Simple "  strong_dep("; Red "set"; Simple ","; Red "pkg1"; Simple ","; Red "pkg2"; Simple ")"; Tab; Underscore "pkg2"; Simple " is a strong dependency of "; Underscore "pkg1"; Simple " in "; Underscore "set"; Newline;
		Simple "  success("; Red "diagnosis"; Simple ")"; Tab; Tab; Simple "Whether there are failures in "; Underscore "diagnosis"; Newline;
    Simple "  trim("; Red "set"; Simple ")"; Tab; Tab; Tab; Simple "Remove all non-installable packages from "; Underscore "set"; Newline;
    Simple "  unit("; Red "package"; Simple ")"; Tab; Tab; Tab; Simple "Give the unit of "; Underscore "package"; Newline;
    Simple "  versions("; Red "unit"; Simple ")"; Tab; Tab; Simple "Give all known versions of a unit"; Newline;
		Simple "  what_provides("; Red "unit"; Simple ")"; Tab; Tab; Simple "Packages that provide "; Underscore "unit"; Newline;
	];;

let help_directives_string =
	colourful_to_string [
		Simple "Directives:"; Newline;
		Simple "  #abundance "; Red "set"; Tab; Tab; Simple "Test a package set for abundance"; Newline;
		Simple "  #dump "; Red "filename"; Simple " "; Red "expression"; Tab; Simple "Dump an expression to a file"; Newline;
		Simple "  #exit"; Tab; Tab; Tab; Tab; Simple "Exit the program"; Newline;
		Simple "  #help"; Tab; Tab; Tab; Tab; Simple "Show help"; Newline;
		Tab; Tab; Tab; Tab; Simple "(#help, #help \"functions\" or #help \"directives\")"; Newline;
		Simple "  #merge "; Red "waterway"; Tab; Tab; Simple "Merge a waterway"; Newline;
		Simple "  #show "; Red "expression"; Tab; Tab; Simple "Print an expression"; Newline;
		Simple "  #type "; Red "expression"; Tab; Tab; Simple "Show the type of "; Underscore "expression"; Newline;
		Simple "  #quit"; Tab; Tab; Tab; Tab; Simple "Exit the program"; Newline;
	];;
