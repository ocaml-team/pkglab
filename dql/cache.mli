(* Copyright 2005-2007 Berke DURAK, Inria Rocquencourt and the EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

module type S =
  sig
    type key
    and thing
    and t
    val create : int option -> t
    val find : t -> key -> thing
    val mem : t -> key -> bool
    val add : t -> key -> thing -> unit
    val remove : t -> key -> unit
  end
;;

module type SingleType =
  sig
    type t
  end
;;

module Make :
  functor (K : Map.OrderedType) ->
    functor (T : SingleType) ->
      sig
        type key = K.t
        and thing = T.t
        type t
        val create : int option -> t
        val decay : t -> unit
        val statistics : t -> int * int * float
        val find : t -> key -> (key -> thing) -> thing
        val remove : t -> key -> unit
        val mem : t -> key -> bool
        val kick : t -> unit
        val add : t -> key -> thing -> unit
      end
;;
