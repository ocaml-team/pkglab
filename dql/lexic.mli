exception Parse_error of int * int * string
val lexical_error : Lexing.lexbuf -> string -> 'a
val token : Lexing.lexbuf -> Syntax.token
