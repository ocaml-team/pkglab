\documentclass[a4paper]{article}
\renewcommand{\familydefault}{pbk}
\newcommand{\codebox}[1]{\fbox{\parbox{\textwidth}{\small \tt #1}}}
%HEVEA\renewcommand{\codebox}[1]{{\tt\begin{tabular}{l}\hline{}#1\\\hline\end{tabular}}}

\title{Pkglab documentation}
\author{Jaap Boender}
\date{April 23, 2009}

\begin{document}
\maketitle

\section{Introduction}

Pkglab is a tool for exploring open-source distributions, currently supporting
Debian-style (Debian, Ubuntu) and RPM-based (Mandriva, Fedora) distributions,
as well as pkgsrc summary files. This document is intended as a guide to its
usage.

The basic mode of operation of Pkglab is to firstly create a universe by
{\em merging} one or more {\em waterways}; a waterway consists of one or more
distributions. Then, the different operations provided by Pkglab (see below)
can be applied to this universe.

\section{Command-line options}

Pkglab has the following command-line options:

\begin{tabular}{|c|c|c|}
\hline
Option & Behaviour & Default value \\
\hline
\hline
{\tt -architecture} & Set the default architecture & {\tt i386} \\
{\tt -history} {\it file} & Use {\it file} for command history & {\tt \$HOME/.pkglab-history} \\
{\tt -script} {\it file} & Execute the script {\it file} & \\
{\tt -merge} {\it waterway} & Merge {\it waterway} & \\
\hline
\end{tabular}

\section{Example session}

We start by merging the Debian Lenny distribution:

\codebox{
./pkglab.opt -merge deb:data/debian/history/2009214-debian-5.0-Packages \\
Completing conflicts...                                            * 100.0\% \\
pkglab version 1.4 by the MANCOOSI Project \\
>}

The {\tt packages} variable contains all the packages in the universe,
currently 22 311:

\codebox{
> count(packages) \\
22311 \\
>}

Let's see if there are any non-installable packages:

\codebox{
> check(packages,packages) \\
Conflicts and dependencies...                                      * 100.0\% \\
Solving                                                            * 100.0\% \\
Conflicts and dependencies...                                      * 100.0\% \\
Solving                                                            * 100.0\% \\
<diagnosis:closure size 22311, 0 failures> \\
>}

There are no non-installable packages.  

\section{Objects and operators}

Pkglab knows several different types of objects:

\begin{description}
  \item[Strings] (denoted: {\tt "\ldots"}) The name of a file or a waterway.
  \item[Units] (denoted: {\tt kde}) A name; combined with a version and an
  architecture it forms a package.
  \item[Packages] (denoted: {\tt kde'5:47@all}) One specific version of a 
  unit, for a specific architecture (or for all architectures).
  \item[Archives] (denoted: {\tt \%name}) A mapping between dates and package
  sets; used to track the contents of a distribution through times
  \item[Dates] (denoted: {\tt YYYY-MM-DD})
  \item[Date ranges] (denoted: {\tt [YYYY-MM-DD;YYYY-MM-DD]})
  \item[Specifications] (denoted: {\tt [. kde (>= 5.2) .]}) A selector
  on the versions of a unit, in this case all versions of the unit {\tt kde}
  greater than or equal to {\tt 5.2}. The version is optional;
  {\tt [. kde .]} is a valid specification as well.
  \item[Sets] (denoted: {\tt \{a, b, \ldots\}})
  \item[Diagnoses] The result of a check operation: a list of packages that
  are not installable with their reasons.
  \item[Functions] (denoted: {\tt \$x -> \ldots \$x \ldots})
  \item[Booleans] (denoted: {\tt true} or {\tt false})
\end{description}

It is possible to use variables: like in Unix shells, variable names are 
prefixed by a dollar sign ({\tt \$}). Assignment is done as follows:

\codebox{
> \$a <- select([. kde (>= 5.2) .])
}

Pkglab has the following operators:

\begin{tabular}{|c|c|}
\hline
Operator & Function \\
\hline
\hline
\tt | & Set union \\
\tt \& & Set intersection \\
\tt \verb+\+ & Set difference \\
\hline
\tt not and or & Boolean operators \\
\hline
\tt =  <> > < >= <= & Comparison (for versions, numbers and sets) \\
\hline
{\it set} \verb+ ~ /+ {\it regexp} {\tt /} & Package selection by regular expression \\
\hline
\end{tabular}

There are inbuilt variables to show all packages, archives, source packages and units in the universe: they are called {\tt packages}, {\tt archives},
{\tt sources} and {\tt units} respectively.

\section{Directives}

\begin{description}
  \item [\#abundance {\it set}] Test {\it set} for abundance; i.e. check if all
  packages have their dependencies satisfied.
  \item [\#dump {\tt filename} {\tt expression}] Dump the result of {\tt expression} to the file {\tt filename}.
  \item [\#exit] Quit Pkglab.
  \item [\#help] Show help (also: {\tt \#help} "functions" or {\tt \#help} "directives")
  \item [\#merge {\it waterway}] Merge {\it waterway} with the universe.
  \item [\#show {\it expression}] Like {\tt \#dump}, but show the expression
  on screen.
  \item [\#type {\it expression}] Show the type of {\it expression}.
  \item [\#quit] The same as {\tt \#exit}.
\end{description}

\section{Functions}

\begin{description}
  \item [check({\it set1}, {\it set2})] Check whether all packages in {\it set1} are installable {\em separately} using only the packages in {\it set2}.  Returns a diagnosis.
  \item [check\_together({\it set1}, {\it set2})] Check whether all packages in {\it set1} are installable {\em together} using only the packages in {\it set2}. Returns a diagnosis.
  \item [closure({\it set})] Give the transitive closure of the dependencies and pre-dependencies of {\it set}, ignoring dependencies that cannot be resolved. Returns a package set.
  \item [conflict\_list({\it package})] Gives the conflict specification of
  {\it package}. Returns a list of specifications.
  \item [conflicts({\it set})] Gives the set of packages that have a conflict with a package in {\it set}. Returns a package set.
  \item [contents({\it archive}, {\it date})] Gives the packages that are in {\it archive} on {\it date}. Returns a package set.
  \item [count({\it set})] Returns the number of elements in {\it set}.
  \item [dep\_closure({\it set})] Like {\tt closure}, but uses only the dependencies of {\it set}.
  \item [dep\_path({\it set}, {\it pkg1}, {\it pkg2})] Shows a dependency path (if available) between {\it pkg1} and {\it pkg2} using only packages from {\tt set}. Returns either Nothing (if no dependency path can be found) or a list of packages.
  \item [depends({\it package})] Gives the dependency specification of {\it package}. Returns a list of lists of version specifications; each list of version specifications represents a disjunction, so that the equivalent of the dependency "A and (B or C)" would be \mbox{{\tt [ [ [. A .] ]; [ [. B .]; [. C .] ] ]}}.
  \item [elements({\it set})] Returns a list that contains the elements of {\it set}.
  \item [exists({\it set}, {\it function})] {\it function} must return a boolean; {\tt exists(x,f)} returns {\tt true} if there is an element in {\tt x} for which {\tt f} returns {\tt true}; {\tt false} otherwise.
  \item [forall({\it set}, {\it function})] Like {\tt exists}, but {\tt forall(x,f)} returns {\tt true} if {\tt f} returns {\tt true} for {\em all} elements of {\tt x}, and {\tt false} otherwise.
  \item [filter({\it set}, {\it function})] {\it function} must return a boolean; {\tt filter(x,f)} returns the set of all elements in {\tt x} for which {\tt f} returns {\tt true}.
  \item [install({\it set1},{\it set2})] If it is possible to install all packages in {\it set1} together using only the packages in {\it set2}, returns a subset of {\it set2} in which all dependencies of {\it set1} are satisfied. Otherwise returns the empty set.
  \item [installed\_size({\it package})] The size after installation of package {\it package}. Returns an integer.
  \item [is\_empty({\it set})] Returns {\tt true} if {\it set} is empty (has no elements). Returns {\tt false} otherwise.
  \item [load\_packages({\it filename})] If the file {\it filename} contains a list of package names (in the form {\tt package'version@arch}), then {\tt load\_packages} returns a package set containing these packages.
  \item [map({\it set},{\it function}] Apply {\it function} to all elements of
  {\it set}. Returns a set of the same type as {\it set}.
  \item [member({\it element}, {\it set})] Check whether {\it element} belongs to {\it set}. Returns a boolean. 
  \item [pre\_closure({\it set})]. Like {\tt closure}, but uses only the pre-dependencies of {\it set}. Returns a package set.
  \item [provides({\it package})] Gives the units {\em explicitly} provided by {\it package}. Returns a list of version specifications.
  \item [range({\it archive})] Gives the dates available in {\it archive}. Returns a list of date ranges (an archive need not be continuous).
  \item [select({\it specification})] Gives the packages that satisfy {\it specification}. Returns a package set.
  \item [size({\it package})] Like {\tt installed\_size}, but gives the size of the package itself (i.e. before installation)
  \item [source({\it package})] Returns the source package from which {\it package} was created.
  \item [strong\_dep({\it set}, {\it package1}, {\it package2})] Checks whether {\it package2} is a strong dependency of {\it package1} in {\it set}; this is the case if (and only if) it is {\em NOT} possible to install {\it package1} in {\it set} without also installing {\tt package2}.
  \item [success({\it diagnosis})] Returns {\tt true} if {\it diagnosis} does not contain any failures, and {\tt false} otherwise.
  \item [trim({\it set})] Given a package set {\it set}, removes the non-installable packages from it (in the context of {\it set}). 
  \item [unit({\it package})] Returns the unit of {\it package}.
  \item [versions({\it unit})] Returns a set of all packages that have {\it unit} as their unit. Returns a package set.
  \item [what\_provides({\it unit})] Gives all packages that provide {\it unit}. Returns a package set.
\end{description}

\end{document}
