include Makefile.config

OCAMLFIND= ocamlfind 
PACKAGE=  -linkpkg -package dose2.conduit,dose2.io,dose2.lifetime,dose2.napkin,dose2.packetology,dose2.rapids,ledit
DESTDIR = /usr/bin
MANDIR = /usr/share/man

pkglab: dql/dql.cma pkglab.ml
ifdef	PROFILE
	$(OCAMLFIND) ocamlcp $(PACKAGE) -I dql -o pkglab $^
else
	$(OCAMLFIND) ocamlc $(PACKAGE) -I dql -o pkglab $^
endif

pkglab.opt: dql/dql.cmxa pkglab.ml
	$(OCAMLFIND) ocamlopt $(PACKAGE) -I dql -o pkglab.opt $^

dql/dql.cma:
	$(MAKE) PROFILE=${PROFILE} -C dql dql.cma

dql/dql.cmxa:
	$(MAKE) -C dql dql.cmxa

install:
	if [ -x pkglab.opt ] ; then \
		cp pkglab.opt $(BINDIR)/pkglab ; \
	elif [ -x pkglab ] ; then \
		cp pkglab $(BINDIR)/pkglab ; \
	else \
		echo "No executable to install found" ; exit 2 ; \
	fi
	test -d $(MANDIR)/man1 || mkdir -p $(MANDIR)/man1
	install -m 644 pkglab.1 $(MANDIR)/man1

clean:
	rm -rf pkglab.cmo pkglab.cmx pkglab pkglab.o pkglab.opt pkglab.cmi
	$(MAKE) -C dql clean
	rm -f dql/.depend

depend:
	touch dql/.depend
	$(MAKE) -C dql depend
