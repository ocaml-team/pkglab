(* Pkglab *)
(* Copyright 2005-2009 Berke DURAK, INRIA Rocquencourt, the EDOS Project and
 * Jaap Boender. *)
(* Released under the GNU GPL version 3 *)

open Conduit
open Rapids
open Interpreter

let sf = Printf.sprintf;;
let db = create_database ();;

(*** Opt *)
module Opt =
  struct
    let architecture = ref "i386"
    (* let workspace = ref ".pkglab-workspace" *)
    let history = ref (Filename.concat (Sys.getenv "HOME") ".pkglab-history")
		let script_file = ref None
		let merge_file = ref None
  end
;;
(* ***)
(*** Specs *)
module Specs =
  struct
    open Opt

    let string key var desc = (key, Arg.String((:=) var), sf "%s (%S)" desc !var);;
    let int key var desc = (key, Arg.Int((:=) var), sf "%s (%d)" desc !var);;
    let string_option key var desc = (key, Arg.String(fun x -> var := Some x), sf "%s" desc);;
    let int_option key var desc = (key, Arg.Int(fun x -> var := Some x), sf "%s" desc);;

    let specs =
      Arg.align
        [
          string "-architecture" architecture " Default architecture";
          (* string "-workspace" workspace " Use given file to load and save workspace"; *)
          string "-history" history " Use given file to load and save command-line history";
					string_option "-script" script_file " Execute commands from given file";
					string_option "-merge" merge_file " Start with merging the given archive";
        ]
    ;;
  end
;;
(* ***)
(*** what *)
let what x =
  Printf.printf "Unwanted argument %S\n" x;
  exit 1
;;
(* ***)

let banner = "pkglab version 1.4.2 by the MANCOOSI Project\n";;

(*** execute_script *)
let execute_script (filename: string) =
let cd = stdoutcd in
let cx = default_batch_context () in
let it = create_interpreter db cx cd in
begin
try
	let f = open_in	filename in
	while true do
		try
			let l = input_line f in
			let q = parse_query l in
			execute_query it q
		with
		| End_of_file -> Printf.printf "\n"; raise Quit
		| Interpreter.Quit -> raise Quit
    | x -> Printf.printf "Exception: %s\n%!" (Printexc.to_string x)
	done	
with
  | Sys_error x -> Printf.printf "Error: %s \n%!" x 
	| Quit -> exit 0
end;;
(* ***)

let read_line =
  let b = Buffer.create 128 in
  fun () ->
    Buffer.clear b;
    let rec loop () =
      let c = Ledit.input_char stdin in
      if c = "\n" then
        Buffer.contents b
      else
        begin
          Buffer.add_string b c;
          loop ()
        end
    in
    loop ()

(*** interactive *)
let interactive () =
  Sys.catch_break true;
  let cx = default_cli_context () in
  let cd = stdoutcd in
  let it = create_interpreter db cx cd in
  (* Ledit.init (); *)
  Ledit.open_histfile false !Opt.history;
  print_string banner;
	flush stdout;
  Ledit.set_prompt "> ";
  try
    while true do
      try
        let l = read_line () in
        let q = parse_query l in
        execute_query it q;
				flush stdout
      with
      | End_of_file ->
          Printf.printf "\n";
          raise Quit
      | Interpreter.Quit -> raise Quit
      | x ->
          Printf.printf "Exception: %s\n%!" (Printexc.to_string x)
    done
  with
  | Quit ->
    Ledit.close_histfile ();
    Printf.printf "Bye.\n%!";
    exit 0
;;
(* ***)

let _ =
  Arg.parse Specs.specs what
    ((Filename.basename Sys.argv.(0)) ^ " [options]");
  (*if !Opt.queries <> [] then Actions.batch ()*)
	(match !Opt.merge_file with
		None -> ()
	| Some x -> Waterway.merge db (Waterway.specification_of_string x));
	match !Opt.script_file with
		None -> interactive ()
	| Some x -> execute_script x
;;
