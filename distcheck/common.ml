(* distcheck *)
(* Copyright 2008-2009 Jaap Boender and the MANCOOSI Project *)
(* Released under the GNU GPL version 3 *)

open Arg
open Diagnosis
open Napkin
open Rapids
open Waterway

let show_successes = ref true
and show_failures = ref true
and explain_results = ref false
and quiet = ref false
and output_xml= ref false
and dist_type = ref `Debian
and source_added = ref false;;

let db = create_database ();;
let architecture_index = get_architecture_index db;;
let unit_index = get_unit_index db;;
let package_index = get_package_index db;;
let not_to_check = ref Package_set.empty;;

let add_source add_to_check s =
let merge x = if !quiet then
	Waterway.merge db ~progress:Progress.dummy x
else
	Waterway.merge db x in
begin
  source_added := true;
	(* This is not very effective, but hey... *)
	let pkgs_old = Functions.packages db in
	(let s2 = if s = "-" then
  begin
    let (n, c) = Filename.open_temp_file "distcheck" "" in
      begin
      try
        while true
        do
          Printf.fprintf c "%s\n" (read_line ())
        done
      with End_of_file -> close_out c
    end;
    n
  end
  else s in
  match !dist_type with
		| `Debian -> merge [Debian, File s2]
		| `RPM -> merge [RPM, File s2]
		| `Pkgsrc -> merge [Pkgsrc, File s2]);
	if not add_to_check then	
	let new_packages = Package_set.diff (Functions.packages db) pkgs_old in
		not_to_check := Package_set.union !not_to_check new_packages	
end;;

let unit_name_of u_id =
	Unit_index.find unit_index u_id;;

let pkg_name_of p_id = 
	let (_, pkg) = Package_index.find package_index p_id in
	let unit_name = Unit_index.find unit_index pkg.pk_unit 
	and version_name = Version_index.get_version (fst pkg.pk_version)
	and release_name = Release_index.get_version (snd pkg.pk_version) in
	Printf.sprintf "%s (= %s%s)" unit_name version_name 
	(match release_name with
		| None -> ""
		| Some rn -> "-" ^ rn);;

let pkg_xml_of p_id =
	let (_, pkg) = Package_index.find package_index p_id in
	let unit_name = Unit_index.find unit_index pkg.pk_unit 
	and arch_name = Architecture_index.find architecture_index pkg.pk_architecture
	and version_name = Version_index.get_version (fst pkg.pk_version)
	and release_name = Release_index.get_version (snd pkg.pk_version) in
	Printf.sprintf "package=\"%s\" architecture=\"%s\" version=\"%s%s\""
		unit_name arch_name version_name 
	(match release_name with
		| None -> ""
		| Some rn -> "-" ^ rn);;

let spec_string s =
let version_string (v, r) =
	let vn = Version_index.get_version v
	and rn = Release_index.get_version r in
	vn ^ (match rn with None -> "" | Some r -> r) in
begin
	match s with
	| Sel_ANY -> ""
	| Sel_LT v -> Printf.sprintf " (&lt; %s)" (version_string v) 
	| Sel_LEQ v -> Printf.sprintf " (&lt;= %s)" (version_string v) 
	| Sel_EQ v -> Printf.sprintf " (= %s)" (version_string v) 
	| Sel_GEQ v -> Printf.sprintf " (&gt;= %s)" (version_string v) 
	| Sel_GT v -> Printf.sprintf " (&gt; %s)" (version_string v) 
end;;

let check () =
let pkgs_to_check = ref (Package_set.diff (Functions.packages db) !not_to_check) in
let result_ht = Hashtbl.create (Package_set.cardinal !pkgs_to_check) in
let progress =
	if !quiet then Progress.dummy
	else new Progress.indicator ~decimation:1000 ~channel:stderr ~label:"Solving..." () in
let diag = Installability.check db ~indicator:progress ~targets:!pkgs_to_check ~available:(Functions.packages db) ()	in
begin
	Array.iter (fun (p_id, rl) ->
		Hashtbl.add result_ht p_id (false, rl);
		pkgs_to_check := Package_set.remove p_id !pkgs_to_check;
	) diag.dg_failures;
	Package_set.iter (fun p_id ->
		Hashtbl.add result_ht p_id (true, [])
	) !pkgs_to_check;
	result_ht
end;;

let show_results ht =
begin
	if !output_xml then print_endline "<results>";
	Hashtbl.iter  
	(fun p_id (result, rl) ->
		if result && !show_successes then
		begin
			if !output_xml then
				Printf.printf "<package %s result=\"success\"/>\n" (pkg_xml_of p_id)
			else
			begin
				Printf.printf "%s: OK\n" (pkg_name_of p_id);
				if !explain_results then
				let (_, pkg) = Package_index.find package_index p_id in
				begin
				Printf.printf "Depends: %s\n" (String.concat ", " (List.map (fun alt ->
					(String.concat " | " (List.map (function
					| Unit_version (u, v) -> (unit_name_of u) ^ (spec_string v)
					| Glob_pattern g -> g) alt)) ^ " {" ^ 
					(String.concat ";" (List.map pkg_name_of (List.flatten (List.map (fun x -> Package_set.elements (Functions.select db x)) alt)))) 
					^ "}"
				) pkg.pk_depends));
				Printf.printf "Pre-depends: %s\n" (String.concat ", " (List.map (fun alt ->
					(String.concat " | " (List.map (function
					| Unit_version (u, v) -> (unit_name_of u) ^ (spec_string v)
					| Glob_pattern g -> g) alt)) ^ " {" ^
					(String.concat ";" (List.map pkg_name_of (List.flatten (List.map (fun x -> Package_set.elements (Functions.select db x)) alt)))) 
					^ "}"
				) pkg.pk_pre_depends));
				Printf.printf "Conflicts: %s\n" (String.concat ", " (List.map (fun c ->
					(match c with
					| Unit_version (u, v) -> (unit_name_of u) ^ (spec_string v)
					| Glob_pattern g -> g) ^ 
					(if Package_set.is_empty (Functions.select db c) then " {ok}" else " {NOT OK}")) pkg.pk_conflicts))
				end
			end
		end
		else if (not result) && !show_failures then
		begin
			if !explain_results then
			begin
				if !output_xml then
					Printf.printf "<package %s result=\"failure\">\n" (pkg_xml_of p_id)
				else
					Printf.printf "%s: FAILED\n" (pkg_name_of p_id);
				List.iter (fun r -> match r with
				| Not_available p_id' -> Printf.printf "  %s is not available\n"
						(pkg_name_of p_id')
				| Requested p_id' -> Printf.printf "  %s has been requested\n"
						(pkg_name_of p_id')
				| Conflict (p_id', p_id'') -> Printf.printf "  %s and %s conflict\n"
						(pkg_name_of p_id') (pkg_name_of p_id'')
				| Empty_disjunction (p_id', rl') -> Printf.printf "  %s depends on missing:\n"
					(pkg_name_of p_id');
					List.iter (fun spec ->
						match spec with
						| Unit_version (u_id, s) -> Printf.printf "  - %s%s\n"
								(unit_name_of u_id) (spec_string s)
						| Glob_pattern g -> Printf.printf "  - %s\n" g
					) rl'	
				| Dependency (p_id', dl') -> Printf.printf "  %s depends on one of:\n"
						(pkg_name_of p_id');
					List.iter (fun p_id'' ->
						Printf.printf "  - %s\n" (pkg_name_of p_id'')
					) dl'	
				) rl;
				if !output_xml then
					print_endline "</package>";
			end
			else
				if !output_xml then
					Printf.printf "<package %s result=\"failure\"/>\n" (pkg_xml_of p_id)
				else
					Printf.printf "%s: FAILED\n" (pkg_name_of p_id)
		end
	) ht;
	if !output_xml then print_endline "</results>";
end;;

let speclist = [
	("-explain", Set explain_results, "Explain the results");
	("-failures", Clear show_successes, "Only show failures");
	("-successes", Clear show_failures, "Only show successes");
	("-base FILE", String (add_source false), "Additional binary package control file providing packages that are not checked but used for resolving dependencies");
	("-quiet", Set quiet, "Do not emit warnings nor progress/timing information");
	("-xml", Set output_xml, "Output results in XML format");
	("-", Unit (fun () -> add_source true "-"), "");
];; 	

let _ =
	if Util.string_contains Sys.argv.(0) "debcheck" then
		dist_type := `Debian
	else if Util.string_contains Sys.argv.(0) "rpmcheck" then
		dist_type := `RPM
	else if Util.string_contains Sys.argv.(0) "pscheck" then
		dist_type := `Pkgsrc
	else (Printf.eprintf "Warning: unknown name '%s', behaving like debcheck\n%!" Sys.argv.(0); dist_type := `Debian);
	Arg.parse speclist (add_source true) "Distcheck v1.4.1";
  if not !source_added then add_source true "-";
	show_results (check ());;
