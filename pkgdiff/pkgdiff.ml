open Napkin
open Rapids
open Waterway

let db = create_database ();;
let archive_index = get_archive_index db;;
let unit_index = get_unit_index db;;

let package_name ?(version_only=false) pkg =
let unit_name = Unit_index.find unit_index pkg.pk_unit in
let (v, r) = pkg.pk_version in
let version_name = Version_index.get_version v in
let release_name = Release_index.get_version r in
	Printf.sprintf "%s%s%s" (if version_only then "" else unit_name ^ "-") version_name
	(match release_name with
	| None -> ""
	| Some r -> "-" ^ r);;

let _ =
begin
  let spec = Sys.argv.(1) 
	and archive = Sys.argv.(2)
	and (y1, m1, d1) = Scanf.sscanf Sys.argv.(3) "%d-%d-%d" (fun y m d -> (y, m, d))
	and (y2, m2, d2) = Scanf.sscanf Sys.argv.(4) "%d-%d-%d" (fun y m d -> (y, m, d)) in
	merge db (specification_of_string spec);
	let a_id = Archive_index.search archive_index archive in
	let a_data = Archive_index.data archive_index a_id in
  prerr_endline "archive found.";
	let ps1 = get_archive_contents a_data (Lifetime.day_of_ymd (y1, m1, d1)) in
  prerr_endline "ps1 found.";
	let ps2 = get_archive_contents a_data (Lifetime.day_of_ymd (y2, m2, d2)) in
  prerr_endline "ps2 found.";
	let d12 = Package_set.diff ps2 ps1 in
  let d21 = Package_set.diff ps1 ps2 in
    Printf.printf "+ %d packages\n- %d packages\n" (Package_set.cardinal d12)
    (Package_set.cardinal d21);
  let u12 = ref Unit_set.empty in
  let u21 = ref Unit_set.empty in
  Package_set.iter (fun p_id -> 
    u12 := Unit_set.add (Functions.get_package_from_id db p_id).pk_unit !u12
  ) d12;
  Package_set.iter (fun p_id -> 
    u21 := Unit_set.add (Functions.get_package_from_id db p_id).pk_unit !u21
  ) d21;
  let depsum = ref 0
  and predepsum = ref 0
  and recmsum = ref 0
  and sugsum = ref 0
  and enhsum = ref 0
  and replsum = ref 0
  and cflsum = ref 0
  and provsum = ref 0 in
  Unit_set.iter (fun u_id ->
    let ps = Functions.unit_id_to_package_set db u_id in
    let p1 = List.hd (List.map (Functions.get_package_from_id db)
    (Package_set.elements (Package_set.inter ps ps1))) in
    let p2 = List.hd (List.map (Functions.get_package_from_id db)
    (Package_set.elements (Package_set.inter ps ps2))) in
    let dep = List.length (List.flatten p2.pk_depends) - List.length
    (List.flatten p1.pk_depends)
    and predep = List.length (List.flatten p2.pk_pre_depends) - List.length
    (List.flatten p1.pk_pre_depends)
    and recm = List.length (List.flatten p2.pk_recommends) - List.length
    (List.flatten p2.pk_recommends)
    and sug = List.length (List.flatten p2.pk_suggests) - List.length
    (List.flatten p2.pk_suggests)
    and enh = List.length (List.flatten p2.pk_enhances) - List.length
    (List.flatten p2.pk_enhances)
    and repl = List.length p2.pk_replaces - List.length p2.pk_replaces
    and cfl = List.length p2.pk_conflicts - List.length p2.pk_conflicts
    and prov = List.length p2.pk_provides - List.length p2.pk_provides
    in
      depsum := !depsum + dep;
      predepsum := !predepsum + predep;
      recmsum := !recmsum + recm;
      sugsum := !sugsum + sug;
      enhsum := !enhsum + enh;
      replsum := !replsum + repl;
      cflsum := !cflsum + cfl;
      provsum := !provsum + prov;
      Printf.printf "%s: %s > %s\nD %+4d PD %+4d RC %+4d S %+4d E %+4d RP %+4d C %+4d PV %+4d [%+6d]\n" (Unit_index.find
      unit_index u_id) (package_name ~version_only:true p1) (package_name
      ~version_only:true p2) dep predep recm sug enh repl cfl prov
      (dep+predep+recm+sug+enh+repl+cfl+prov)
  ) (Unit_set.inter !u12 !u21);
  Unit_set.iter (fun u_id ->
    let ps = Functions.unit_id_to_package_set db u_id in
    let p = List.hd (List.map (Functions.get_package_from_id db)
    (Package_set.elements (Package_set.inter ps ps2))) in
    let dep = List.length (List.flatten p.pk_depends)
    and predep = List.length (List.flatten p.pk_pre_depends)
    and recm = List.length (List.flatten p.pk_recommends)
    and sug = List.length (List.flatten p.pk_suggests)
    and enh = List.length (List.flatten p.pk_enhances)
    and repl = List.length p.pk_replaces
    and cfl = List.length p.pk_conflicts
    and prov = List.length p.pk_provides
    in
      depsum := !depsum + dep;
      predepsum := !predepsum + predep;
      recmsum := !recmsum + recm;
      sugsum := !sugsum + sug;
      enhsum := !enhsum + enh;
      replsum := !replsum + repl;
      cflsum := !cflsum + cfl;
      provsum := !provsum + prov;
      Printf.printf "new package: %s\nD %+4d PD %+4d RC %+4d S %+4d E %+4d RP %+4d C %+4d PV %+4d [%+6d]\n" (package_name p) dep predep recm sug enh repl cfl prov
      (dep+predep+recm+sug+enh+repl+cfl+prov)
  ) (Unit_set.diff !u12 !u21);
  Unit_set.iter (fun u_id ->
    let ps = Functions.unit_id_to_package_set db u_id in
    let p = List.hd (List.map (Functions.get_package_from_id db)
    (Package_set.elements (Package_set.inter ps ps1))) in
    let dep = -(List.length (List.flatten p.pk_depends))
    and predep = -(List.length (List.flatten p.pk_pre_depends))
    and recm = -(List.length (List.flatten p.pk_recommends))
    and sug = -(List.length (List.flatten p.pk_suggests))
    and enh = -(List.length (List.flatten p.pk_enhances))
    and repl = -(List.length p.pk_replaces)
    and cfl = -(List.length p.pk_conflicts)
    and prov = -(List.length p.pk_provides)
    in
      depsum := !depsum + dep;
      predepsum := !predepsum + predep;
      recmsum := !recmsum + recm;
      sugsum := !sugsum + sug;
      enhsum := !enhsum + enh;
      replsum := !replsum + repl;
      cflsum := !cflsum + cfl;
      provsum := !provsum + prov;
      Printf.printf "removed package: %s\nD %+4d PD %+4d RC %+4d S %+4d E %+4d RP %+4d C %+4d PV %+4d [%+6d]\n" (package_name p) dep predep recm sug enh repl cfl prov
      (dep+predep+recm+sug+enh+repl+cfl+prov)
  ) (Unit_set.diff !u21 !u12);
  Printf.printf "D %+4d PD %+4d RC %+4d S %+4d E %+4d RP %+4d C %+4d PV %+4d [%+6d]\n" !depsum !predepsum !recmsum !sugsum !enhsum !replsum !cflsum !provsum
      (!depsum + !predepsum + !recmsum + !sugsum + !enhsum + !replsum + !cflsum + !provsum)
end;;
